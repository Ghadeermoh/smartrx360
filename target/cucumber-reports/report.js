$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/gmohamed/Smartrx360Group/src/main/java/com/Smartrx/features/MesVentesGroupment.feature");
formatter.feature({
  "line": 1,
  "name": "MesVentesGroupment tile testing",
  "description": "",
  "id": "mesventesgroupment-tile-testing",
  "keyword": "Feature"
});
formatter.before({
  "duration": 34301285100,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "User opens MesventesGroupmentTile",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": ": user at MesVentesGroupment tile",
  "keyword": "Given "
});
formatter.match({
  "location": "MesVentesStepDefinition.user_at_MesVentesGroupment_tile()"
});
formatter.result({
  "duration": 1940869400,
  "status": "passed"
});
formatter.scenario({
  "line": 39,
  "name": "As a user I can select any value from period filter",
  "description": "",
  "id": "mesventesgroupment-tile-testing;as-a-user-i-can-select-any-value-from-period-filter",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 38,
      "name": "@MVG-f-02"
    }
  ]
});
formatter.step({
  "line": 40,
  "name": ": select any value from period filter and compare output",
  "keyword": "Then "
});
formatter.match({
  "location": "MesVentesStepDefinition.select_any_value_from_period_filter_and_compare_output()"
});
formatter.result({
  "duration": 158602240500,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.SmartrxStepDefinitions.MesVentesStepDefinition.select_any_value_from_period_filter_and_compare_output(MesVentesStepDefinition.java:71)\r\n\tat ✽.Then : select any value from period filter and compare output(C:/Users/gmohamed/Smartrx360Group/src/main/java/com/Smartrx/features/MesVentesGroupment.feature:40)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 728249000,
  "status": "passed"
});
});