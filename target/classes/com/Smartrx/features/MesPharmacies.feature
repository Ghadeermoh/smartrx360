Feature: MesPharmacies tile testing

Background: User opens MesPharmacies Tile
Given : user at MesPharmacies tile

@MP-D-01
Scenario:  Verify page title 
Then : check tile title

@MP-D-02
Scenario: verify number of pharmacies in tile 
Then : check number of pharmacies in tile

@MP-D-03
Scenario:
Scenario:verify groupment name of MesPharmacies tile
Then : check groupment name of MesPharmacies tile

#@MP-D-03
#Scenario: verify default columns in table header by default 
#Then : check table column header used by default in mes pharmacies tile

@MP-O-01
Scenario: As a user I can see applied filter by default in the tile 
Then : check filters selected by default 

@MP-f-01
Scenario: As a user I can select any value from period filter
Then : select any value from period filter in mes pharmacies and check Used filter name if exist

@MP-fs-01
Scenario Outline:As a user I can use specific period in mes pharmacies to compare data by entering dates in boxes with different formats
Then : enter "<date debut>" and "<date fin>" in mes pharmacies period filter and check Used filter name if exist
Examples:
|date debut|date fin|
|12-3-2020|15-8-2020|
|12/3/2020|15/8/2020|

@MP-f-02
Scenario: As a user I can select any value from evolution filter
Then : select any value from evolution filter in mes pharmacies and check data output

@MP-fs-02
Scenario Outline:As a user I can use specific evolution in mes pharmacies to compare data by entering dates in boxes with different formats
Then : enter "<date debut>" and "<date fin>" in evolution filter and compare data in mes pharmacies tile
Examples:
|date debut|date fin|
|12-3-2020|15-8-2020|
|12/3/2020|15/8/2020|

@C-fs-p
Scenario Outline: As a user I can use search functionality in pharmacies filter to get values more easy
Then : Check if suggested values related to "<search_value>" in mes pharmacies pharmacy filter
And : Check if data output based on "<search_value>"  related to "<selected_value>" in mes pharmacies pharmacy filter
Examples:
|search_value|selected_value|
|MEN|PHARMACIE MENARD|
|ENV|ENVOL-PCV-FLOIRAC SARL|

@C-fs-po
Scenario: As a user I select any value from pharmacies filter in mes pharmacies tile
Then : select any value from pharmacies filter and check data output

@C-fs-pd
Scenario: As a user I can not find duplicated pharmacy values in pharmacy filter inside mes Pharmacies tile
Then : check duplicated values in pharmacies filter inside mes pharmacies tile

@C-fs-M
Scenario Outline: As a user I can use search functionality in Marches filter to get values more easy
Then : Check if suggested values related to "<search_value>" in mes pharmacies marche filter
And : Check if data output based on "<search_value>"  related to "<selected_value>" in mes pharmacies marche filter
Examples:
|search_value|selected_value|
|AVE|Avene|
|ENV|ENVOL-PCV-FLOIRAC SARL|

@MP-LDT
Scenario Outline: As a user I can see last date transmission with specific colors based on date
Then :check last transmission cells color based on "<today date>"
Examples:
|today date|
|10/11/2020|

@MP_ED_01
Scenario: As a user I can download data in Mes pharmacies tile in csv format 
Then : check Mes pharmacies data download in csv format

@MP_ED_02
Scenario: As a user I can download data in Mes pharmacies tile in pdf format 
And : check Mes pharmacies data download in pdf format

@MP-f-TVA
Scenario: As a user I can use Taux TVA filter to control data in mes pharmacies tile
Then : select any value in TauxTVA filter in mes pharmacies tile and check data output

@MP-f-FBCB
Scenario: As a user I can use FamilleBCB filter to control data in mes pharmacies tile
Then : select any value in FamilleBCB in mes pharmacies tile and check data output

@MP-f-ATC
Scenario: As a user I can use ClasseATC filter to control data in mes pharmacies tile
Then : select any value in ClasseATC in mes pharmacies tile and check data output

@MP-f-Seg
Scenario: As a user I can use segmentation filter to control data in mes pharmacies tile
Then : select any value in segmentation in mes pharmacies tile and check data output

@MP-fs-LB1
Scenario Outline:As a user I can not search with less than three characters 
When :I search with "<search_check_limit>" less than three characters it gives me an error message 
Examples:
|search_check_limit|
|M|
|US|

@MP-fs-LB2
Scenario Outline: As a user I can use search functionality in LaboratoireBCB filter to get specific values
Then : Check if suggested values related to "<search_value>" in mes pharmacies LaboratoireBCB filter
And : check data output based on "<search value>" and "<selected value>" in LaboratoireBCB filter in mes pharmacies
Examples:
|search_value|selected_value|
|UPS|UPSA|
|MYL|MYLAN MEDICAL|

@MP-f-M1
Scenario Outline:As a user I can not search with less than three characters in Marque filter
When :I search with "<search_check_limit>" less than three characters in marque filter in mes pharmacies it gives me an error message 
Examples:
|search_check_limit|
|M|
|US|

@MP-f-M2
Scenario Outline: As a user I can use Marque search to filter data output based on selected Marque
Then : Make sure suggested values related to marque filter "<search value>" in Mes pharmacies
And : check data output based on "<search value>" and "<selected value>" in marque filter mes pharmacies
Examples:
|search value|selected value|
|DOLI|DOLIPRANE|
|LAMA|CYCLAMAX|

@MP-f-NDP1
Scenario Outline:As a user I can not search with less than three characters in Nom du produit filter
When :I search with "<search_check_limit>" less than three characters in Nom du produit filter in mes pharmacies it gives me an error message 
Examples:
|search_check_limit|
|M|
|US|

@MP-f-NDP2
Scenario Outline: As a user I can use Marque search to filter data output based on selected Marque
Then : Make sure suggested values related to Nom du produit filter "<search value>" in Mes pharmacies
And : check data output based on "<search value>" and "<selected value>" in Nom du produit filter mes pharmacies
Examples:
|search value|selected value|
|DAFA|DAFALGAN 1 000MG CPR 8|
|STF|JOBSTFOAM 5CMX9CM|

@MP-f-G&P
Scenario: As a user I can use Generic and Princepes filter to control data in mes pharmacies tile
Then : select any value in Generic and Princepes filter in mes pharmacies tile and check data output

@MP-f-TDV
Scenario: As a user I can use Type de ventes filter to control data in mes pharmacies tile
Then : select any value in Type de ventes filter in mes pharmacies tile and check data output

@MP-f-OTC
Scenario: As a user I can use Médicament OTC filter to control data in mes pharmacies tile
Then : select any value in Médicament OTC filter in mes pharmacies tile and check data output

#@Scenario19
#Scenario: As a user I can select any value from Etat de donnees filter in Mespharmacies tile
#Then : select any value from Etat de donnees filter in Mespharmacies tile and compare output
#And : check all filter options in Mespharmacies with each period filter option

#@Jira545
#Scenario Outline: As a user I can create saved filters in MesPharmacies
#When :  MesPharmacies filter option  "<selected_option>" selected from filter
#Then : create  saved filter with "<type>"  and "<name>" and check if created successfully in MesPharmacies
#And : use this filter "<name>" as a default filter and verify "<selected_option>" applied by default in MesPharmacies
#Then : check if creator can delete this filter "<name>" in MesPharmacies
#Examples:
#|type|name|selected_option|
#|private|ghadeerprivate|Période précédente|
#|shared|ghadeershared|Période précédente|
