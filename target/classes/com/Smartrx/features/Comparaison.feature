Feature: Comparaison tile testing

Background: User Open Comparaison tile  
Given : User at Comparaison tile

@C-DT1
Scenario: Verify page title
Then : check Name of Comparaison tile

@C-DT2
Scenario: Verify default filters
Then : check default filters selections

@C-DT3
Scenario: Verify groupment name
Then : check groupment name in the tile

@C-DT4
Scenario: Verify default table buttons
Then : check default table buttons selections

@C-f-CS
Scenario: As a user I can select any filter option in comparison segment filter 
Then : select one of the comparison segment filters and check Data output

@C-f-Gr
Scenario: As a user I can select any filter option in groupment filter
When  : I select groupment option in comparison segment filter
Then : comparison groupment filter is displayed and I can check default value in it
And : select any option in comparison groupment filter and check data output

@C-f-P
Scenario: As a user I can select any filter option in period filter
Then : select one of the period filters and check Data output

@C-f-RP
Scenario: As a user I can select any filter option in reference period filter
Then : select one of the reference period filters and check Data output


@C-f-Ph
Scenario: As a user I can select any filter option in pharmacy filter
Then : select one of the pharmacy filter options and check Data output

#And : Make sure pharmacy filter options not duplicated 

@C-fS-Ph
Scenario Outline: As a user I can search for specific value in comparison tile pharmacy filter
Then : Make sure suggested values related to "<search_val>" in Comparison tile Pharmacies filter
And : Make sure data output related to "<search_val>" and "<selected_val>" in Comparison tile Pharmacies filter
Examples:
|search_val|selected_val|
|MAV|PHARMACIE MAVIA|
|EUR|EURL GRANDE PHARMACIE PAGET BR|

@C-E-01&C-E-02
Scenario: As a user I can download file in two formats csv and pdf
Then : check download in csv format
And : check download in pdf format

@C-TB
Scenario: As a user I can use buttons at the top of table to filter data in table
Then : select each value of the buttons exist at the top of each table and compare data output

@C-OA-0P
Scenario: As a user I can use period filter with analysis type MDL
When : I select analysis type Comparatif de marge par tranche MDL
Then : select any value from period filter and check data output

@C-OA-CS
Scenario: As a user I can use comparison segment filter with analysis type MDL
When : I select analysis type Comparatif de marge par tranche MDL
Then : select any value from comparison segment filter and check data output

@C-OAD-03
Scenario Outline:As a user I can use specific period in comparison to compare data by entering dates in boxes with different formats
When : I select analysis type Comparatif de marge par tranche MDL
Then : enter "<date debut>" and "<date fin>" and compare data output in comparison tile with analysis filter 
Examples:
|date debut|date fin|
|12-3-2020|15-8-2020|
|12/3/2020|15/8/2020|


@C-OA-0Ph
Scenario: As a user I can use pharmacy filter with analysis type MDL
When : I select analysis type Comparatif de marge par tranche MDL
Then : select any value from pharmacy filter and check data output

