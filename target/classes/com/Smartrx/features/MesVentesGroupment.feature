Feature: MesVentesGroupment tile testing

Background: User opens MesventesGroupmentTile
Given : user at MesVentesGroupment tile

@MVG-D1
Scenario:  Verify page title 
Then : check Name of MesVentesGroupment title
@MVG-D2
Scenario: Verify number of pharmacies 
Then : check number of pharmacies of MesVentesGroupment title
@MVG-D3
Scenario: verify groupement name of MesVentesGroupment title
Then :check groupment name

@MVG-D4
Scenario: verify table header of MesVentesGroupment title
Then : check default table header

@MVG-f-D
Scenario: verify MesVentesGroupment default filters selections
Then : check MesVentesGroupment default filters selections

@MVG-O_01
Scenario: As a user I can select any value from analysis type filter
Then : select any value from analysis type filter and compare output

@MVG-OS_01
Scenario Outline: As a user I can use search functionality in analysis filter to get values more easy
Then : use search in analysis filter to get specific values by entering "<search_value>" and "<selected_value>"
Examples:
|search_value|selected_value|
|prod|Résultats par type de produit|
|marq|Résultats par marque (Top 1000)|
|taux|Résultats par taux de TVA|


@MVG-f-02
Scenario: As a user I can select any value from period filter
Then : select any value from period filter and compare output 

@MVG-fdp-02
Scenario Outline:As a user I can use specific period to compare data by entering dates in boxes with different formats
Then : enter "<date debut>" and "<date fin>" in period date personalisee and compare data 
Examples:
|date debut|date fin|
|12-3-2020|15-8-2020|
|12/3/2020|15/8/2020|

@MVG-f-03
Scenario: As a user I can select any value from evolution filter
Then : select any value from evolution filter and compare output

@MVG-fdp-03
Scenario Outline:As a user I can use specific evolution period to compare data by entering dates in boxes with different formats
Then : enter "<date debut>" and "<date fin>" in evolution date personalisee and compare data 
Examples:
|date debut|date fin|
|12-3-2020|15-8-2020|
|12/3/2020|15/8/2020|

@MVG_ECSV_01
Scenario: As a user I can download data in csv and pdf format
Then : check mesventes data download in csv format
And : check mesventes data download in pdf format

@MVG_ECSV_03
Scenario Outline: As a user I can download data with details in CSV format from burger Icon
Then : open burger Icon and select "<details>" to add it to downloadded file 
Examples:
|details|
|Résultats par laboratoire exploitant|

@MVG-f_06
Scenario: As a user I can select any value from TauxTVA filter
When :I select analysis type with TauxTVA
Then : I can select any value from TauxTVA filter and compare output

@MVG-f-7
Scenario: As a user I can filter data using GénériqueAndprinceps filter
When : I select analysis type by princeps
Then : I can select any value from GénériqueAndprinceps filter options and compare data output

@MVG-f-8
Scenario: As a user I can filter data using TypeDeVentes filter
When : I select analysis type by TypeDeVentes
Then : I can select any value from TypeDeVentes filter options and compare data output


@MVG-fs-Pharmacy
Scenario Outline: As a user I can use search functionality in pharmacies filter to get values more easy
When : I select analysis type with pharmacies
Then : Make sure suggested values in pharmacy filter related to "<search value>" 
And : Make sure data output in pharmacy filter related to "<search value>" and "<selected value>"
Examples:
|search value|selected value|
|ENV|ENVOL-PCV-FLOIRAC SARL|
|EURL|EURL GRANDE PHARMACIE PAGET BR|

@MVG-fS_Marche
Scenario Outline: As a user I can use search functionality in marche filter to get values more easy
Then : Make sure suggested values in marche related to "<search value>" 
And : Make sure data output in marche related to "<search value>" and "<selected value>"
Examples:
|search value|selected value|
|3 ch|Creation marche 736|
|derm|A-DERMA|

@MVG-f-OTC
Scenario: As a user I can use OTC filter to control data output
When : I select analysis type with type de produit
Then : I select each option in OTC filter and compare data output

@MVG-f-PC
Scenario: As a user I can use produitCher filter to control data output
When : I select analysis type with type de produit
Then : I select each option in produitCher filter and compare data output
 

@MVG-f-FBCB
Scenario: As a user I can use familleBCB filter to control data output
Then : select any value from famileBCB filter and compare output

@MVG-f-Seg
Scenario: As a user I can use segmentation filter to control data output
When : I select analysis type with segmentation
Then : select any value from segmentation filter and compare output

@MVG-f-IT
Scenario Outline: As a user I can use search functionality in Indications thérapeutiques filter to get values more easy
When : I select analysis type with Indications thérapeutiques
Then : Make sure suggested values in Indications thérapeutiques filter related to "<search value>" 
And : Make sure data output in Indications thérapeutiques filter related to "<search value>" and "<selected value>"
Examples:
|search value|selected value|
|Douleur|Douleur articulaire|

@Scenario18
Scenario: As a user I can use setting button to add some columns like"part de marche"
Then : select value from setting and check existence in table 
And :deselect value from setting and check

@MVG-f-LBCB
Scenario Outline:As a user I can not search with less than three characters in laboratoire BCB filter
When :I search with "<search_check_limit>" less than three charactersin laboratoire BCB filter it gives me an error message 
Examples:
|search_check_limit|
|M|
|US|

@MVG-f-LBCB1
Scenario Outline: As a user I can use Laboratoire BCB search to filter data output based on selected Laboratoire 
When : I select analysis type with laboratoire exploitant 
Then : Make sure suggested values related to "<search value>" 
And : Make sure data output related to "<search value>" and "<selected value>"
Examples:
|search value|selected value|
|A-DERMA|A-DERMA|
|BIOD|BIODIM|
|MYLAN|MYLAN|

@MVG-f-LBCB2
Scenario Outline: As a user I can use buttons in the Laboratoire BCB filter to control data output
When : I select analysis type with laboratoire exploitant 
Then :check button which control dataoutput values based on "<selected value>"
And : check button which control type of Laboratories based on "<selected value>"
Examples:
|selected value|
|MYLAN|

@MVG-f-M
Scenario Outline:As a user I can not search with less than three characters in Marque filter
When :I search with "<search_check_limit>" less than three characters in marque filter in mesVentes  it gives me an error message 
Examples:
|search_check_limit|
|M|
|US|

@MVG-f-M1
Scenario Outline: As a user I can use Marque search to filter data output based on selected Marque
When : I select analysis type with maqrue 
Then : Make sure suggested values related to marque filter "<search value>" in mesVentes
And : Make sure data output related to marque filter "<search value>" and "<selected value>" in mesVentes
Examples:
|search value|selected value|
|DOLI|DOLIPRANE|
|LAMA|CYCLAMAX|

@MVG-f-M2
Scenario: As a user I can use buttons in the Marque filter to control data output
When : I select analysis type with maqrue 
Then :check button which control dataoutput values in mesVentes

@MVG-f-NDP
Scenario Outline: As a user I can not search with less than three characters in nom du produit filter
When :I search with "<search_check_limit>" less than three characters in nom du produit it gives me error message
Examples:
|search_check_limit|
|M|
|US|

@MVG-f-NDP1
Scenario Outline: As a user I can use nom du produit search to filter data output based on selected produit
When :I select analysis type with produit
Then : Make sure suggested values related to nom du produit filter "<search value>" 
And : Make sure data output related to nom du produit filter "<search value>" and "<selected value>"
Examples:
|search value|selected value|
|DAFA|DAFALGAN 1 000MG CPR 8|
|STF|JOBSTFOAM 5CMX9CM|

@MVG-f-NDP2
Scenario: As a user I can use buttons in the NomDUProduit filter to control data output
When :I select analysis type with produit
Then :check button option which control data Output values in nom du produit filter 

@MVG-f-CS
Scenario: As a user I can select any value from comparison segment filter
Then : select any value from comparison segment filter and compare output

@MVG-f-G
Scenario: As a user I can select any value from groupment filter
When : I select analysis type with pharmacies
Then : select any value from groupment filter and compare output

@MVG-fS-FL
Scenario Outline: As a user I can use search functionality in my favorite laboratory filter to get values more easy
Then : use search in my favorite laboratory filter to get specific values by entering "<search_value>" and "<selected_value>"
Examples:
|search_value|selected_value|
|mylan|mylan|
|top|top1|

@MVG-fs-GG
Scenario Outline: As a user I can use search functionality in groupe generic filter to display specific value
When : I select analysis type with groupe generic 
Then : Make sure suggested values related to groupe generic filter "<search value>"
And : Make sure data output related to groupe generic filter "<search value>" and "<selected value>"
Examples:
|search value|selected value|
|ZOPHREN|ZOPHREN / ONDANSETRON (CHLORHYDRATE D') / 2 mg/ml / Préparation injectable|

@MVG-fs-GG1
Scenario: As a user I can use Groupe generic button to control data output
Then :check button option which control data Output values in groupe generic filter

#@MVG-f_04
#Scenario: As a user I can select any value from marche filter
#Then : select any value from marche filter and compare output

#@MVG-f_08
#Scenario: As a user I can select any value from favorite laboratory filter 
#Then : select any value from favorite laboratory filter and check data output

#@Scenario7
#Scenario: As a user I can select any value from pharmacies filter
#Then : select any value from pharmacies filter and compare output

#@Jira653
#Scenario: As a user I can select any value from Etat de donnees filter
#Then : select any value from Etat de donnees filter and compare output
#And : check all filter options with each period filter option
#Then : check all filter options with pharmacy filter 



#staging Scenarios

#@Jira545
#Scenario Outline: As a user I can select filter to create as a saved filter 
#When : filter option  "<selected_option>" selected from filter
#Then : create  saved filter with "<type>"  and "<name>" and check if created successfully
#And : use this filter "<name>" as a default filter and verify "<selected_option>" applied by default
#Then : check if creator can delete this filter "<name>"
#Examples:
#|type|name|selected_option|
#|private|ghadeerprivate|Aujourd'hui|
#|shared|ghadeershared|Aujourd'hui|
