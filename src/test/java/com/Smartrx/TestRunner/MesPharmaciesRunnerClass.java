package com.Smartrx.TestRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "C:\\Users\\gmohamed\\Smartrx360Group\\src\\main\\java\\com\\Smartrx\\features\\MesPharmacies.feature"
, glue = "com.SmartrxStepDefinitions"
,plugin = { "json:target/cucumber.json", "pretty","html:target/cucumber-reports" }
, dryRun =false , monochrome = true
//,tags= {"@MP-LDT"}
)
public class MesPharmaciesRunnerClass {

}
