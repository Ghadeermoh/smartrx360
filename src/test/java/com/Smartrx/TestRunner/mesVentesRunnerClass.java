package com.Smartrx.TestRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "C:\\Users\\gmohamed\\Smartrx360Group\\src\\main\\java\\com\\Smartrx\\features\\MesVentesGroupment.feature"
, glue = "com.SmartrxStepDefinitions"
, plugin = {"pretty","html:target/cucumber-reports" }
, dryRun =false , monochrome = true
,tags= {"@MVG-f-02"}
)

public class mesVentesRunnerClass {
}
