package com.Smartrx.TestRunner;

import org.junit.runner.RunWith;

import com.Smartrx.Pages.BaseClass;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions (features="C:\\Users\\gmohamed\\Smartrx360Group\\src\\main\\java\\com\\Smartrx\\features\\ObservatoireDesPrix.feature" 
, glue="com.SmartrxStepDefinitions" 
,plugin = { "pretty", "junit:target/cucumber-reports/Cucumber.xml" }   //add report 
,dryRun=false       
,monochrome=true
,tags= {"@Scenario3"}
)

public class ObservatoireRunnerClass extends BaseClass{
	
	
}
