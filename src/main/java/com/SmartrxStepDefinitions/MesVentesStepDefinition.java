package com.SmartrxStepDefinitions;

import java.util.List;

import org.junit.Assert;

import com.Smartrx.Pages.BaseClass;
import com.Smartrx.Pages.HomePage;
import com.Smartrx.Pages.MesVentesGroupment;
import com.Smartrx.Utility.MesVentesData;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MesVentesStepDefinition extends BaseClass {

	public static HomePage home_page;
	public static MesVentesGroupment mes_ventes_groupment;

	// ---------background--------------------
	@Given("^: user at MesVentesGroupment tile$")
	public void user_at_MesVentesGroupment_tile() {
		mes_ventes_groupment = new MesVentesGroupment();
		mes_ventes_groupment.Open_mes_ventes_groupment_Tile();
	}

	// ------------Scenario1------------------
	@Then("^: check Name of MesVentesGroupment title$")
	public void check_Name_of_MesVentesGroupment_title() {
		String title = mes_ventes_groupment.verify_tile_title();
		Assert.assertEquals(MesVentesData.Expected_mesventes_title, title);
	}

	@Then("^: check number of pharmacies of MesVentesGroupment title$")
	public void check_number_of_pharmacies_of_MesVentesGroupment_title() {
		String numberofPharmacies = mes_ventes_groupment.verify_numer_of_pharmacies();
		Assert.assertEquals(MesVentesData.Expected_mesventes_pharmacies, numberofPharmacies);
	}

	@Then("^:check groupment name$")
	public void check_groupment_name() {
		String groupmentName = mes_ventes_groupment.verify_groupment_name();
		Assert.assertEquals(MesVentesData.Expected_mesventes_groupment, groupmentName);
	}

	@Then("^: check default table header$")
	public void check_default_table_header() {
		MesVentesGroupment.verify_table_columns();
	}

	// ---------Scenario2--------------
	@Then("^: check MesVentesGroupment default filters selections$")
	public void check_MesVentesGroupment_default_filters_selections() {
		mes_ventes_groupment.verify_default_filters_selection();
//		mes_ventes_groupment.verify_table_columns_comparisonSwitch();
	}

	// ------------Scenario3----------------------
	@Then("^: select any value from analysis type filter and compare output$")
	public void select_any_value_from_analysis_type_filter_and_compare_output() {
		mes_ventes_groupment.verify_analysis_type_filter();
	}

	// ---------Scenario4-------------
	@Then("^: select any value from period filter and compare output$")
	public void select_any_value_from_period_filter_and_compare_output() {
		List<Boolean> period_filter_check = mes_ventes_groupment.verify_period_filter_options();
		for (int i = 0; i < period_filter_check.size(); i++) {
			Assert.assertTrue(period_filter_check.get(i));
		}
	}

	// ----------------------------------------------------------------------
	@Then("^: enter \"([^\"]*)\" and \"([^\"]*)\" in period date personalisee and compare data$")
	public void enter_and_in_period_date_personalisee_and_compare_data(String arg1, String arg2) {
		mes_ventes_groupment.verify_specific_period_selection(arg1, arg2);
	}

	// -----Scenario5---------------
	@Then("^: select any value from evolution filter and compare output$")
	public void select_any_value_from_evolution_filter_and_compare_output() {
		List<Boolean> filter_check = mes_ventes_groupment.verify_evolution_filter_options();
		for (int i = 0; i < filter_check.size(); i++) {
			System.out.println(filter_check.get(i));
			Assert.assertTrue(filter_check.get(i));
		}
	}

	@Then("^: enter \"([^\"]*)\" and \"([^\"]*)\" in evolution date personalisee and compare data$")
	public void enter_and_in_evolution_date_personalisee_and_compare_data(String arg1, String arg2) {

	}

	// ---Scenario6---------------
	@Then("^: select any value from marche filter and compare output$")
	public void select_any_value_from_marche_filter_and_compare_output() {
		mes_ventes_groupment.verify_marche_filter_options();
	}

	// ---------Scenario7---------------
	@Then("^: select any value from pharmacies filter and compare output$")
	public void select_any_value_from_pharmacies_filter_and_compare_output() {
		mes_ventes_groupment.verify_pharmacies_filter_options();
	}

	// ---------Scenario8---------------------
	@Then("^: select any value from comparison segment filter and compare output$")
	public void select_any_value_from_comparison_segment_filter_and_compare_output() {
		List<Boolean> comparison_options_check = mes_ventes_groupment.verify_comparison_segment_filter_options();
		for (int i = 0; i < comparison_options_check.size(); i++) {
			Assert.assertTrue(comparison_options_check.get(i));
		}

	}

	// --------Scenario9-----------------
	@Then("^: select any value from groupment filter and compare output$")
	public void select_any_value_from_groupment_filter_and_compare_output() {
		List<Boolean> groupment_check = mes_ventes_groupment.verify_groupment_filter_options();
		for (int i = 0; i < groupment_check.size(); i++) {
			System.out.println(groupment_check.get(i));
			Assert.assertTrue(groupment_check.get(i));
		}

	}

	// --------Scenario10-----------------
	@Then("^: check mesventes data download in csv format$")
	public void check_mesventes_data_download_in_csv_format() {
		mes_ventes_groupment.verify_mesventes_downloadCSV();
	}

	@Then("^: check mesventes data download in pdf format$")
	public void check_mesventes_data_download_in_pdf_format() {
		mes_ventes_groupment.verify_mesventes_downloadPDF();
	}

	// ---------------ScenarioMVG_ECSV_03------------
	@Then("^: open burger Icon and select \"([^\"]*)\" to add it to downloadded file$")
	public void open_burger_Icon_and_select_to_add_it_to_downloadded_file(String arg1) {
		mes_ventes_groupment.verify_download_CSV_withDetails(arg1);
	}

	// --------Scenario11---------------------
	@When("^: filter option  \"([^\"]*)\" selected from filter$")
	public void filter_option_selected_from_filter(String arg1) {
		mes_ventes_groupment.select_filter_tobe_saved(arg1);
	}

	@Then("^: create  saved filter with \"([^\"]*)\"  and \"([^\"]*)\" and check if created successfully$")
	public void create_saved_filter_with_and_and_check_if_created_successfully(String arg1, String arg2) {
		mes_ventes_groupment.verify_adding_saved_filters(arg1, arg2);
	}

	@Then("^: use this filter \"([^\"]*)\" as a default filter and verify \"([^\"]*)\" applied by default$")
	public void use_this_filter_as_a_default_filter_and_verify_applied_by_default(String arg1, String arg2) {
		mes_ventes_groupment.verify_using_savedFilter_Asdefault(arg1, arg2);
	}

	@Then("^: check if creator can delete this filter \"([^\"]*)\"$")
	public void check_if_creator_can_delete_this_filter(String arg1) {
		mes_ventes_groupment.verify_delete_saved_filters(arg1);
	}

	// ------Scenario12-------------
	@When("^:I select analysis type with TauxTVA$")
	public void i_select_analysis_type_with_TauxTVA() {
		mes_ventes_groupment.select_analysisType_withTauxTVA();
	}

	@Then("^: I can select any value from TauxTVA filter and compare output$")
	public void i_can_select_any_value_from_TauxTVA_filter_and_compare_output() {
		List<Boolean> filter_otions_check = mes_ventes_groupment.verify_TauxTVA_filter_options();
		for (int i = 0; i < filter_otions_check.size(); i++) {
			System.out.println(filter_otions_check.get(i));
			Assert.assertTrue(filter_otions_check.get(i));
		}
	}

	// ----------Scenario Generic and princeps------
	@When("^: I select analysis type by princeps$")
	public void i_select_analysis_type_by_princeps() {
		mes_ventes_groupment.selectAnalysis_resultByprincepes();
	}

	@Then("^: I can select any value from GénériqueAndprinceps filter options and compare data output$")
	public void i_can_select_any_value_from_GénériqueAndprinceps_filter_options_and_compare_data_output() {
		List<Boolean> Princepes = mes_ventes_groupment.verify_GénériqueAndprinceps_filter_options();
		for (int i = 0; i < Princepes.size(); i++) {
			Assert.assertTrue(Princepes.get(i));
		}
	}

	// -----Scenario Type de ventes filter---------------
	@When("^: I select analysis type by TypeDeVentes$")
	public void i_select_analysis_type_by_TypeDeVentes() {
		mes_ventes_groupment.selectAnalysis_resultByTypeDeVentes();
	}

	@Then("^: I can select any value from TypeDeVentes filter options and compare data output$")
	public void i_can_select_any_value_from_TypeDeVentes_filter_options_and_compare_data_output() {
		List<Boolean> Typedevente = mes_ventes_groupment.verify_TypeDeVentes_filter_options();
		for (int i = 0; i < Typedevente.size(); i++) {
			Assert.assertTrue(Typedevente.get(i));
		}
	}

	// -----------Scenario-MVG-OS_01---------------
	@Then("^: use search in analysis filter to get specific values by entering \"([^\"]*)\" and \"([^\"]*)\"$")
	public void use_search_in_analysis_filter_to_get_specific_values_by_entering_and(String arg1, String arg2) {
		mes_ventes_groupment.verify_analysis_filter_search(arg1, arg2);
	}

	// --------pharmacy filter search-------------
	@When("^: I select analysis type with pharmacies$")
	public void i_select_analysis_type_with_pharmacies() {
		mes_ventes_groupment.select_analysisResults_bypharmacies();
	}

	@Then("^: Make sure suggested values in pharmacy filter related to \"([^\"]*)\"$")
	public void make_sure_suggested_values_in_pharmacy_filter_related_to(String arg1) {
		List<Boolean> pharmacies_suggestedValus = mes_ventes_groupment.verify_pharmacies_suggestedValues(arg1);
		for (int i = 0; i < pharmacies_suggestedValus.size(); i++) {
			Assert.assertTrue(pharmacies_suggestedValus.get(i));
		}
	}

	@Then("^: Make sure data output in pharmacy filter related to \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_sure_data_output_in_pharmacy_filter_related_to_and(String arg1, String arg2) {
		List<Boolean> pharmacy_data_check = mes_ventes_groupment.verify_pharmacy_selected_values(arg1, arg2);
		for (int i = 0; i < pharmacy_data_check.size(); i++) {
			Assert.assertTrue(pharmacy_data_check.get(i));
		}
	}

	// ----------Scenaerio-MVG-fS_04-------------
	@Then("^: Make sure suggested values in marche related to \"([^\"]*)\"$")
	public void make_sure_suggested_values_in_marche_related_to(String arg1) {
		List<Boolean> check_marche_suggestedValus = mes_ventes_groupment.verify_marche_filter_suggestedValus(arg1);
		for (int i = 0; i < check_marche_suggestedValus.size(); i++) {
			Assert.assertTrue(check_marche_suggestedValus.get(i));
		}
	}

	@Then("^: Make sure data output in marche related to \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_sure_data_output_in_marche_related_to_and(String arg1, String arg2) {
		// Write code here that turns the phrase above into concrete actions
	}

	// ------------------------------------------
	@Then("^: select any value from favorite laboratory filter and check data output$")
	public void select_any_value_from_favorite_laboratory_filter_and_check_data_output() {
		// Write code here that turns the phrase above into concrete actions
	}

	// -----------Sceanrio16---------------------
	@Then("^: use search in my favorite laboratory filter to get specific values by entering \"([^\"]*)\" and \"([^\"]*)\"$")
	public void use_search_in_my_favorite_laboratory_filter_to_get_specific_values_by_entering_and(String arg1,
			String arg2) {
		mes_ventes_groupment.verify_myFavoriteLab_filter_search(arg1, arg1);
	}

	// -----------FamilleBCB filter ---------------------
	@Then("^: select any value from famileBCB filter and compare output$")
	public void select_any_value_from_famileBCB_filter_and_compare_output() throws Exception {
		List<Boolean> FBCB_check = mes_ventes_groupment.verify_FamileBCB_filter_options();
		for (int i = 0; i < FBCB_check.size(); i++) {
			System.out.println(FBCB_check.get(i));
			Assert.assertTrue(FBCB_check.get(i));
		}
	}

	// -----------------Segmentation-------------------
	@When("^: I select analysis type with segmentation$")
	public void i_select_analysis_type_with_segmentation() {
		mes_ventes_groupment.select_analysisType_withSegmentation();
	}

	@Then("^: select any value from segmentation filter and compare output$")
	public void select_any_value_from_segmentation_filter_and_compare_output() {
		List<Boolean> segmentation_check = mes_ventes_groupment.verify_Segmentation_filter_options();
		for (int i = 0; i < segmentation_check.size(); i++) {
			System.out.println(segmentation_check.get(i));
			Assert.assertTrue(segmentation_check.get(i));
		}
	}

	// ---------Scenario18--------------
	@Then("^: select value from setting and check existence in table$")
	public void select_value_from_setting_and_check_existence_in_table() {
		mes_ventes_groupment.verify_partmarche_values_selection();
	}

	@Then("^:deselect value from setting and check$")
	public void deselect_value_from_setting_and_check() {
//		mes_ventes_groupment.verify_partmarche_value_deselection();
	}

	// --------Scenario19-----------------------
	@Then("^: select any value from Etat de donnees filter and compare output$")
	public void select_any_value_from_Etat_de_donnees_filter_and_compare_output() {
		mes_ventes_groupment.select_analysisResults_bypharmacies();
		mes_ventes_groupment.verify_Etat_de_donnees_filter();
	}

	@Then("^: check all filter options with each period filter option$")
	public void check_all_filter_options_with_each_period_filter_option() {
		mes_ventes_groupment.select_analysisResults_bypharmacies();
		mes_ventes_groupment.verify_EtatDeDonees_with_periodFilter();
	}

	@Then("^: check all filter options with pharmacy filter$")
	public void check_all_filter_options_with_pharmacy_filter() {
		mes_ventes_groupment.verify_EtatDeDonees_with_pharmacyFilter();
	}

	// -----------------------Scenario-MVG-f_08-----------------------------
	@When("^:I search with \"([^\"]*)\" less than three charactersin laboratoire BCB filter it gives me an error message$")
	public void i_search_with_less_than_three_charactersin_laboratoire_BCB_filter_it_gives_me_an_error_message(
			String arg1) {
		Boolean check_characterLimit = mes_ventes_groupment.verify_charactersLimit_search(arg1);
		Assert.assertTrue(check_characterLimit);
	}

	@When("^: I select analysis type with laboratoire exploitant$")
	public void i_select_analysis_type_with_laboratoire_exploitant() {
		mes_ventes_groupment.select_analysisResults_byLaboratoire();
	}

	@Then("^: Make sure suggested values related to \"([^\"]*)\"$")
	public void make_sure_suggested_values_related_to(String arg1) {
		List<Boolean> BCB_suggestedValues_check = mes_ventes_groupment.verify_Laboratoire_BCB_suggestedValues(arg1);
		for (int i = 0; i < BCB_suggestedValues_check.size(); i++) {
			System.out.println(BCB_suggestedValues_check.get(i));
			Assert.assertTrue(BCB_suggestedValues_check.get(i));
		}
	}

	@Then("^: Make sure data output related to \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_sure_data_output_related_to_and(String arg1, String arg2) {
		List<Boolean> BCBdataoutput_check = mes_ventes_groupment.verify_Adding_laboratoireBCB_filter(arg1, arg2);
		for (int i = 0; i < BCBdataoutput_check.size(); i++) {
			System.out.println(BCBdataoutput_check.get(i));
			Assert.assertTrue(BCBdataoutput_check.get(i));
		}
	}

	@Then("^:check button which control dataoutput values based on \"([^\"]*)\"$")
	public void check_button_which_control_dataoutput_values_based_on(String arg1) {
		// Write code here that turns the phrase above into concrete actions
	}

	@Then("^: check button which control type of Laboratories based on \"([^\"]*)\"$")
	public void check_button_which_control_type_of_Laboratories_based_on(String arg1) {
		// Write code here that turns the phrase above into concrete actions
	}

	// ---------Scenario marque filter----------
	@When("^:I search with \"([^\"]*)\" less than three characters in marque filter in mesVentes  it gives me an error message$")
	public void i_search_with_less_than_three_characters_in_marque_filter_in_mesVentes_it_gives_me_an_error_message(
			String arg1) {
		Boolean minimum_char_check = mes_ventes_groupment.verify_marque_search_character_limits(arg1);
		Assert.assertTrue(minimum_char_check);
	}

	@When("^: I select analysis type with maqrue$")
	public void i_select_analysis_type_with_maqrue() {
		mes_ventes_groupment.selectAnalysis_resultByMarque();
	}

	@Then("^: Make sure suggested values related to marque filter \"([^\"]*)\" in mesVentes$")
	public void make_sure_suggested_values_related_to_marque_filter_in_mesVentes(String arg1) {
		List<Boolean> marque_suggestedValue_check = mes_ventes_groupment.verify_marque_filter_suggestedValues(arg1);
		for (int i = 0; i < marque_suggestedValue_check.size(); i++) {
			Assert.assertTrue(marque_suggestedValue_check.get(i));
		}
	}

	@Then("^: Make sure data output related to marque filter \"([^\"]*)\" and \"([^\"]*)\" in mesVentes$")
	public void make_sure_data_output_related_to_marque_filter_and_in_mesVentes(String arg1, String arg2) {
		List<Boolean> MarqueData_check = mes_ventes_groupment.verify_marque_added_filter(arg1, arg2);
		for (int i = 0; i < MarqueData_check.size(); i++) {
			Assert.assertTrue(MarqueData_check.get(i));
		}
	}

	@Then("^:check button which control dataoutput values in mesVentes$")
	public void check_button_which_control_dataoutput_values_in_mesVentes() {
		// Write code here that turns the phrase above into concrete actions
	}

	// ---------------Scenario nom du produit----------------
	@When("^:I search with \"([^\"]*)\" less than three characters in nom du produit it gives me error message$")
	public void i_search_with_less_than_three_characters_in_nom_du_produit_it_gives_me_error_message(String arg1) {
		Boolean NomDuroduit_check = mes_ventes_groupment.verify_NomDeproduit_search_character_limits(arg1);
		Assert.assertTrue(NomDuroduit_check);
	}

	@When("^:I select analysis type with produit$")
	public void i_select_analysis_type_with_produit() {
		mes_ventes_groupment.selectAnalysis_resultByproduit();
	}

	@Then("^: Make sure suggested values related to nom du produit filter \"([^\"]*)\"$")
	public void make_sure_suggested_values_related_to_nom_du_produit_filter(String arg1) {
		List<Boolean> NomDuProduit_suggestedVals = mes_ventes_groupment
				.verify_NomDuproduit_filter_suggestedValues(arg1);
		for (int i = 0; i < NomDuProduit_suggestedVals.size(); i++) {
			Assert.assertTrue(NomDuProduit_suggestedVals.get(i));
		}
	}

	@Then("^: Make sure data output related to nom du produit filter \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_sure_data_output_related_to_nom_du_produit_filter_and(String arg1, String arg2) {
		List<Boolean> NomDuProduit_data = mes_ventes_groupment.verify_NomDuproduit_added_filter(arg1, arg2);
		for (int i = 0; i < NomDuProduit_data.size(); i++) {
			Assert.assertTrue(NomDuProduit_data.get(i));
		}
	}

	@Then("^:check button option which control data Output values in nom du produit filter$")
	public void check_button_option_which_control_data_Output_values_in_nom_du_produit_filter() {
		// Write code here that turns the phrase above into concrete actions
	}

	// ---------Scenario groupe generic-----------
	@When("^: I select analysis type with groupe generic$")
	public void i_select_analysis_type_with_groupe_generic() {
		mes_ventes_groupment.selectAnalysis_resultByGroupeGeneric();
	}

	@Then("^: Make sure suggested values related to groupe generic filter \"([^\"]*)\"$")
	public void make_sure_suggested_values_related_to_groupe_generic_filter(String arg1) {
		List<Boolean> groupGeneric_suggestedValus = mes_ventes_groupment.verify_groupeGeneric_suggestedValus(arg1);
		for (int i = 0; i < groupGeneric_suggestedValus.size(); i++) {
			Assert.assertTrue(groupGeneric_suggestedValus.get(i));
		}
	}

	@Then("^: Make sure data output related to groupe generic filter \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_sure_data_output_related_to_groupe_generic_filter_and(String arg1, String arg2) {
		List<Boolean> groupGeneric_dataoutput = mes_ventes_groupment.verify_groupegeneric_filter_add(arg1, arg2);
		for (int i = 0; i < groupGeneric_dataoutput.size(); i++) {
			Assert.assertTrue(groupGeneric_dataoutput.get(i));
		}
	}

	@Then("^:check button option which control data Output values in groupe generic filter$")
	public void check_button_option_which_control_data_Output_values_in_groupe_generic_filter() {
		// Write code here that turns the phrase above into concrete actions

	}

	// -----------OTC filter-----------------
	@When("^: I select analysis type with type de produit$")
	public void i_select_analysis_type_with_type_de_produit() {
		mes_ventes_groupment.selectAnalysisType_withTypedeProduit();
	}

	@Then("^: I select each option in OTC filter and compare data output$")
	public void i_select_each_option_in_OTC_filter_and_compare_data_output() {
		Boolean check = mes_ventes_groupment.verify_MedicamentOTC_filter_options();
		Assert.assertTrue(check);
	}

	// --------------produitCher>----------------------------------------------------
	@Then("^: I select each option in produitCher filter and compare data output$")
	public void i_select_each_option_in_produitCher_filter_and_compare_data_output() {
		Boolean check = mes_ventes_groupment.verify_ProduitCher_filter_options();
		Assert.assertTrue(check);
	}

	// --------------Indication thera-----------------------
	@When("^: I select analysis type with Indications thérapeutiques$")
	public void i_select_analysis_type_with_Indications_thérapeutiques() {
		mes_ventes_groupment.selectAnalysisType_withIndicationthérapeutiques();
	}

	@Then("^: Make sure suggested values in Indications thérapeutiques filter related to \"([^\"]*)\"$")
	public void make_sure_suggested_values_in_Indications_thérapeutiques_filter_related_to(String arg1) {
		List<Boolean> Indication_check = mes_ventes_groupment.verify_Indicationthérapeutiques_suggestedValus(arg1);
		for (int i = 0; i < Indication_check.size(); i++) {
			Assert.assertTrue(Indication_check.get(i));
		}
	}

	@Then("^: Make sure data output in Indications thérapeutiques filter related to \"([^\"]*)\" and \"([^\"]*)\"$")
	public void make_sure_data_output_in_Indications_thérapeutiques_filter_related_to_and(String arg1, String arg2) {
		List<Boolean> IndicationData_check = mes_ventes_groupment.verify_Indicationthérapeutiques_dataOutput(arg1,
				arg2);
		for (int i = 0; i < IndicationData_check.size(); i++) {
			Assert.assertTrue(IndicationData_check.get(i));
		}

	}

}
