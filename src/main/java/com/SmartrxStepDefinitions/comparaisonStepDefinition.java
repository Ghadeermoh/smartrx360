package com.SmartrxStepDefinitions;

import java.io.File;
import java.util.List;

import org.junit.Assert;

import com.Smartrx.Pages.BaseClass;
import com.Smartrx.Pages.Comparaison;
import com.Smartrx.Pages.HomePage;
import com.Smartrx.Utility.ComparisonData;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class comparaisonStepDefinition extends BaseClass {
	public static HomePage home_page;
	public static Comparaison comparaison_page;

	@Before
	public void setup() {
		init();
		home_page = new HomePage();
		home_page.Login(prop.getProperty("username"), prop.getProperty("password"));
//		home_page.groument_selection(prop.getProperty("Groupement"));
	}

	// --------background--------------
	@Given("^: User at Comparaison tile$")
	public void user_at_Comparaison_tile() {
		comparaison_page = new Comparaison();
		comparaison_page.open_comparaison_tile();
	}

	// ---------Default data --------------
	@Then("^: check Name of Comparaison tile$")
	public void check_Name_of_Comparaison_tile() {
		String tile_name = comparaison_page.verify_tile_title();
		Assert.assertEquals(ComparisonData.Expected_comparaison_title, tile_name);
	}

	@Then("^: check default filters selections$")
	public void check_default_filters_selections() {
		comparaison_page.verify_default_filters_selection();
	}

	@Then("^: check groupment name in the tile$")
	public void check_groupment_name_in_the_tile() {
		String group_name = comparaison_page.verify_groupment_name();
		Assert.assertEquals(ComparisonData.groumentName, group_name);
	}

	@Then("^: check default button selections$")
	public void check_default_button_selections() {
		comparaison_page.verify_default_buttons_selections();
	}

	@Then("^: check default table buttons selections$")
	public void check_default_table_buttons_selections() {
		comparaison_page.verify_default_buttons_selections();
	}

	// -------------table buttons------------
	@Then("^: select each value of the buttons exist at the top of each table and compare data output$")
	public void select_each_value_of_the_buttons_exist_at_the_top_of_each_table_and_compare_data_output() {
		comparaison_page.verify_tables_buttons();
	}

	// ----------Period filter--------------
	@Then("^: select one of the period filters and check Data output$")
	public void select_one_of_the_period_filters_and_check_Data_output() {
		comparaison_page.verify_period_filter_options();
	}

	// -------comparison segment-----------
	@Then("^: select one of the comparison segment filters and check Data output$")
	public void select_one_of_the_comparison_segment_filters_and_check_Data_output() {
		comparaison_page.verify_comparison_segmant_filter_options();
	}

	// --------------------groupment filter ---------------
	@When("^: I select groupment option in comparison segment filter$")
	public void i_select_groupment_option_in_comparison_segment_filter() {
		comparaison_page.select_comparison_segment_withGroupment();
	}

	@Then("^: comparison groupment filter is displayed and I can check default value in it$")
	public void comparison_groupment_filter_is_displayed_and_I_can_check_default_value_in_it() {
		comparaison_page.verify_groupment_filter_default_option();
	}

	@Then("^: select any option in comparison groupment filter and check data output$")
	public void select_any_option_in_comparison_groupment_filter_and_check_data_output() {
		comparaison_page.verify_groupment_filter_options();
	}

	// -------analysis with period and pharmacy filter filter --------
	@When("^: I select analysis type Comparatif de marge par tranche MDL$")
	public void i_select_analysis_type_Comparatif_de_marge_par_tranche_MDL() {
		comparaison_page.select_analysisType_withMDL();
	}

	@Then("^: select any value from period filter and check data output$")
	public void select_any_value_from_period_filter_and_check_data_output() {
		comparaison_page.verify_period_filter_optionsanalysis2();
	}

	@Then("^: enter \"([^\"]*)\" and \"([^\"]*)\" and compare data output in comparison tile with analysis filter$")
	public void enter_and_and_compare_data_output_in_comparison_tile_with_analysis_filter(String arg1, String arg2) {
		comparaison_page.verify_date_personalisee(arg1, arg2);
	}

	@Then("^: select any value from pharmacy filter and check data output$")
	public void select_any_value_from_pharmacy_filter_and_check_data_output() {
		comparaison_page.verify_pharmacy_filter_optionsanalysisMDL();
	}

	// ---------------reference period filter-----------------------
	@Then("^: select one of the reference period filters and check Data output$")
	public void select_one_of_the_reference_period_filters_and_check_Data_output() {
		comparaison_page.verify_referenceperiod_filter_options();
	}

	// -------Pharmacies filter --------
	@Then("^: select one of the pharmacy filter options and check Data output$")
	public void select_one_of_the_pharmacy_filter_options_and_check_Data_output() {
		comparaison_page.verify_pharmacy_filter_options();
	}

//	@Then("^: Make sure pharmacy filter options not duplicated$")
//	public void make_sure_pharmacy_filter_options_not_duplicated() {
//		comparaison_page.verify_pharmacy_filter_dupliaction();
//	}

	// -----------------pharmacy filter search----------
	@Then("^: Make sure suggested values related to \"([^\"]*)\" in Comparison tile Pharmacies filter$")
	public void make_sure_suggested_values_related_to_in_Comparison_tile_Pharmacies_filter(String arg1) {
		List<Boolean> check_search=comparaison_page.verify_pharmacy_searchSuggestedVals(arg1);
		for(int i=0;i<check_search.size();i++) {
			Assert.assertTrue(check_search.get(i));
		}
	}
	@Then("^: Make sure data output related to \"([^\"]*)\" and \"([^\"]*)\" in Comparison tile Pharmacies filter$")
	public void make_sure_data_output_related_to_and_in_Comparison_tile_Pharmacies_filter(String arg1, String arg2) {
		comparaison_page.verify_pharmacy_selectedVal(arg1, arg2);
	}

	// ----------export data -----------------
	@Then("^: check download in csv format$")
	public void check_download_in_pdf_format() {
		comparaison_page.verify_csvfiles_download();
	}

	@Then("^: check download in pdf format$")
	public void check_download_in_csv_format() {
		comparaison_page.verify_pdfFile_download();
	}

	@After
	public void teardown() {
		driver.quit();
		for (File file : folder.listFiles()) {
			file.delete();
		}
		folder.delete();
	}

}
