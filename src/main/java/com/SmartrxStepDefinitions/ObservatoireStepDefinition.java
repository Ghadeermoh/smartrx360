package com.SmartrxStepDefinitions;

import com.Smartrx.Pages.BaseClass;
import com.Smartrx.Pages.ObservatoireDesPrix;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class ObservatoireStepDefinition extends BaseClass {
	public ObservatoireDesPrix obseratoire_desPrix;
	
	//---------background----------------
	@Given("^: User at Observaroire des prix tile$")
	public void user_at_Observaroire_des_prix_tile() {
		obseratoire_desPrix=new ObservatoireDesPrix();
		obseratoire_desPrix.open_observatoire_tile();
	   
	}
    //-------Scenario1----------------------
	@Then("^: check name of Observaroire des prix tile$")
	public void check_name_of_Observaroire_des_prix_tile() {
		obseratoire_desPrix.verify_tile_name();
	}
    
	//------------Scenario2----------------------
	@Then("^: check default filters selection$")
	public void check_default_filters_selection() {
		obseratoire_desPrix.verify_default_filters();
	}
	
	//-------Scenario3--------
	@Then("^: select one of the options in marche filter and check data output$")
	public void select_one_of_the_options_in_marche_filter_and_check_data_output() {
		obseratoire_desPrix.verify_marche_filter_options();
	}

}
