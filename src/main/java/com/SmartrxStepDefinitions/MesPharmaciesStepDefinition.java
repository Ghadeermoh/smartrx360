package com.SmartrxStepDefinitions;

import java.text.ParseException;
import java.util.List;
import org.junit.Assert;
import com.Smartrx.Pages.BaseClass;
import com.Smartrx.Pages.HomePage;
import com.Smartrx.Pages.MesPharmacies;
import com.Smartrx.Utility.MesPharmaciesData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MesPharmaciesStepDefinition extends BaseClass {
	public static MesPharmacies mes_pharmacies;
	public static HomePage home_page;

	// ---------background--------------------
	@Given("^: user at MesPharmacies tile$")
	public void user_at_MesPharmaciest_tile() {
		mes_pharmacies = new MesPharmacies();
		mes_pharmacies.Open_Mes_pharmacies_tile();
	}

	// -----------Scenario-------------------
	@Then("^: check tile title$")
	public void check_tile_title() {
		 String page_title=mes_pharmacies.verify_tile_title();
		 Assert.assertEquals(MesPharmaciesData.tile_name, page_title);
	}

	@Then("^: check number of pharmacies in tile$")
	public void check_number_of_pharmacies_in_tile() {
		String number_of_pharmacies = mes_pharmacies.verify_numer_of_pharmacies();
		Assert.assertEquals(MesPharmaciesData.number_of_pharmacies, number_of_pharmacies);
	}

	@Then("^: check groupment name of MesPharmacies tile$")
	public void check_groupment_name_of_MesPharmacies_tile() {
		String groupment = mes_pharmacies.verify_groupment_name();
		Assert.assertEquals(MesPharmaciesData.groupment_name, groupment);
	}

	@Then("^: check table column header used by default in mes pharmacies tile$")
	public void check_table_column_header_used_by_default_in_mes_pharmacies_tile() {
		List<String> table_columns = mes_pharmacies.verify_table_columns();
		for (int i = 0; i < MesPharmaciesData.default_column_header.length; i++) {
			Assert.assertEquals(MesPharmaciesData.default_column_header[i], table_columns.get(i));
		}

	}

	// --------Scenario default filters ----------------
	@Then("^: check filters selected by default$")
	public void check_filters_selected_by_default() {
		List<String> filter_selected = mes_pharmacies.verify_default_filters_selection();
		for (int i = 0; i < filter_selected.size(); i++) {
			Assert.assertEquals(filter_selected.get(i), MesPharmaciesData.Expected_mespharmacies_default_filters[i]);
		}

	}

	// ---------Scenario period filter----------------
	@Then("^: select any value from period filter in mes pharmacies and check Used filter name if exist$")
	public void select_any_value_from_period_filter_in_mes_pharmacies_and_check_Used_filter_name_if_exist() {
		List<Boolean> period_filter_withTable = mes_pharmacies.check_period_filter_options();
		for (int i = 0; i < period_filter_withTable.size(); i++) {
			Assert.assertTrue(period_filter_withTable.get(i));
		}
	}

	@Then("^: enter \"([^\"]*)\" and \"([^\"]*)\" in mes pharmacies period filter and check Used filter name if exist$")
	public void enter_and_in_mes_pharmacies_period_filter_and_check_Used_filter_name_if_exist(String arg1,
			String arg2) {
		mes_pharmacies.verify_specific_period_selection(arg1, arg2);
	}

	// ----------Scenario evolution filter--------------
	@Then("^: select any value from evolution filter in mes pharmacies and check data output$")
	public void select_any_value_from_evolution_filter_in_mes_pharmacies_and_check_data_output() {
		List<Boolean> evolution_check = mes_pharmacies.verify_evolution_filter_options();
		for (int i = 0; i < evolution_check.size(); i++) {
			Assert.assertTrue(evolution_check.get(i));
		}
	}

	@Then("^: enter \"([^\"]*)\" and \"([^\"]*)\" in evolution filter and compare data in mes pharmacies tile$")
	public void enter_and_in_evolution_filter_and_compare_data_in_mes_pharmacies_tile(String arg1, String arg2) {
		mes_pharmacies.verify_specific_evolution_selection(arg1, arg2);
	}

	// -----------Scenario11------------------
	@When("^:  MesPharmacies filter option  \"([^\"]*)\" selected from filter$")
	public void mespharmacies_filter_option_selected_from_filter(String arg1) {
		mes_pharmacies.select_filter_tobe_saved(arg1);
	}

	@Then("^: create  saved filter with \"([^\"]*)\"  and \"([^\"]*)\" and check if created successfully in MesPharmacies$")
	public void create_saved_filter_with_and_and_check_if_created_successfully_in_MesPharmacies(String arg1,
			String arg2) {
		mes_pharmacies.verify_adding_saved_filters(arg1, arg1);

	}

	@Then("^: use this filter \"([^\"]*)\" as a default filter and verify \"([^\"]*)\" applied by default in MesPharmacies$")
	public void use_this_filter_as_a_default_filter_and_verify_applied_by_default_in_MesPharmacies(String arg1,
			String arg2) {
		mes_pharmacies.verify_using_savedFilter_Asdefault(arg1, arg2);
	}

	@Then("^: check if creator can delete this filter \"([^\"]*)\" in MesPharmacies$")
	public void check_if_creator_can_delete_this_filter_in_MesPharmacies(String arg1) {
		mes_pharmacies.verify_delete_saved_filters(arg1);
	}

	// ------------Scenario19------------------
	@Then("^: select any value from Etat de donnees filter in Mespharmacies tile and compare output$")
	public void select_any_value_from_Etat_de_donnees_filter_in_Mespharmacies_tile_and_compare_output() {
		mes_pharmacies.verify_Etat_de_donnees_filter();
	}

	@Then("^: check all filter options in Mespharmacies with each period filter option$")
	public void check_all_filter_options_in_Mespharmacies_with_each_period_filter_option() {
		mes_pharmacies.verify_EtatDeDonees_with_periodFilter();
	}

	// --------------Export data----------------------
	@Then("^: check Mes pharmacies data download in csv format$")
	public void check_Mes_pharmacies_data_download_in_csv_format() {
		mes_pharmacies.verify_MesPharmacies_downloadCSV();
	}

	@Then("^: check Mes pharmacies data download in pdf format$")
	public void check_Mes_pharmacies_data_download_in_pdf_format() {
		mes_pharmacies.verify_MesPharmacies_downloadPDF();
	}

	// ---------------pharmacy filter------------------------
	@Then("^: select any value from pharmacies filter and check data output$")
	public void select_any_value_from_pharmacies_filter_and_check_data_output() {
		List<Boolean> PharmaciesFilter_options = mes_pharmacies.verify_pharmacies_filter_options();
		for (int i = 0; i < PharmaciesFilter_options.size(); i++) {
			System.out.println(PharmaciesFilter_options.get(i));
			Assert.assertTrue(PharmaciesFilter_options.get(i));
		}
	}

	@Then("^: check duplicated values in pharmacies filter inside mes pharmacies tile$")
	public void check_duplicated_values_in_pharmacies_filter_inside_mes_pharmacies_tile() {
		List<Boolean> PharmaciesFilter_duplication = mes_pharmacies.checkPharmacy_filterduplicated_options();
		for (int i = 0; i < PharmaciesFilter_duplication.size(); i++) {
			Assert.assertTrue(PharmaciesFilter_duplication.get(i));
		}
	}

	// ----------------search in pharmacy filter ---------------
	@Then("^: Check if suggested values related to \"([^\"]*)\" in mes pharmacies pharmacy filter$")
	public void check_if_suggested_values_related_to_in_mes_pharmacies_pharmacy_filter(String arg1) {
		List<Boolean> check_suggestedVal = mes_pharmacies.verify_pharmacies_filter_search_suggestedValues(arg1);
		for (int i = 0; i < check_suggestedVal.size(); i++) {
			Assert.assertTrue(check_suggestedVal.get(i));
		}
	}

	@Then("^: Check if data output based on \"([^\"]*)\"  related to \"([^\"]*)\" in mes pharmacies pharmacy filter$")
	public void check_if_data_output_based_on_related_to_in_mes_pharmacies_pharmacy_filter(String arg1, String arg2) {
		List<Boolean> check_dataOutput = mes_pharmacies.Verify_pharmacies_filter_search_selectedValues(arg1, arg2);
		for (int i = 0; i < check_dataOutput.size(); i++) {
			Assert.assertTrue(check_dataOutput.get(i));
		}
	}

	// --------Search in Marche filter-------------------
	@Then("^: Check if suggested values related to \"([^\"]*)\" in mes pharmacies marche filter$")
	public void check_if_suggested_values_related_to_in_mes_pharmacies_marche_filter(String arg1) {
		List<Boolean> check_suggestedVal = mes_pharmacies.verify_marches_filter_search_suggestedValues(arg1);
		for (int i = 0; i < check_suggestedVal.size(); i++) {
			Assert.assertTrue(check_suggestedVal.get(i));
		}
	}

	@Then("^: Check if data output based on \"([^\"]*)\"  related to \"([^\"]*)\" in mes pharmacies marche filter$")
	public void check_if_data_output_based_on_related_to_in_mes_pharmacies_marche_filter(String arg1, String arg2) {

	}

	// --------------last date transmission-----------------
	@Then("^:check last transmission cells color based on \"([^\"]*)\"$")
	public void check_last_transmission_cells_color_based_on(String arg1) throws ParseException {
		mes_pharmacies.verify_lastDateTransmission(arg1);
	}

	// -----------TauxTVA filter------------------
	@Then("^: select any value in TauxTVA filter in mes pharmacies tile and check data output$")
	public void select_any_value_in_TauxTVA_filter_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> TVA_check = mes_pharmacies.verify_TVA_filter_options();
		for (int i = 0; i < TVA_check.size(); i++) {
			Assert.assertTrue(TVA_check.get(i));
		}
	}

	// --------------FamilleBCB---------------------
	@Then("^: select any value in FamilleBCB in mes pharmacies tile and check data output$")
	public void select_any_value_in_FamilleBCB_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> FamilleBCB_check = mes_pharmacies.verify_familleBCB_filter_options();
		for (int i = 0; i < FamilleBCB_check.size(); i++) {
			Assert.assertTrue(FamilleBCB_check.get(i));
		}
	}

	// ------------ClasseATC-----------------------
	@Then("^: select any value in ClasseATC in mes pharmacies tile and check data output$")
	public void select_any_value_in_ClasseATC_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> ClasseATC_check = mes_pharmacies.verify_ClasseATC_filter_options();
		for (int i = 0; i < ClasseATC_check.size(); i++) {
			Assert.assertTrue(ClasseATC_check.get(i));
		}
	}

	// ---------------Segmentation------------------
	@Then("^: select any value in segmentation in mes pharmacies tile and check data output$")
	public void select_any_value_in_segmentation_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> segmentation_check = mes_pharmacies.verify_Segmentation_filter_options();
		for (int i = 0; i < segmentation_check.size(); i++) {
			Assert.assertTrue(segmentation_check.get(i));
		}
	}

	// -----------Laboratoire BCB---------------------------
	@When("^:I search with \"([^\"]*)\" less than three characters it gives me an error message$")
	public void i_search_with_less_than_three_characters_it_gives_me_an_error_message(String arg1) {
		Boolean ErrorMessageDisplayed = mes_pharmacies.verify_LaboratoireBCB_search_character_limits(arg1);
		Assert.assertTrue(ErrorMessageDisplayed);
	}

	@Then("^: Check if suggested values related to \"([^\"]*)\" in mes pharmacies LaboratoireBCB filter$")
	public void check_if_suggested_values_related_to_in_mes_pharmacies_LaboratoireBCB_filter(String arg1) {
		List<Boolean> suggested_values = mes_pharmacies.verify_LaboratoireBCB_filter_search_suggestedValues(arg1);
		for (int i = 0; i < suggested_values.size(); i++) {
			Assert.assertTrue(suggested_values.get(i));
		}
	}

	@Then("^: check data output based on \"([^\"]*)\" and \"([^\"]*)\" in LaboratoireBCB filter in mes pharmacies$")
	public void check_data_output_based_on_and_in_LaboratoireBCB_filter_in_mes_pharmacies(String arg1, String arg2) {
		List<Boolean> check_data=mes_pharmacies.verify_LaboratoireBCB_filter_addedValues(arg1, arg2);
		for (int i = 0; i < check_data.size(); i++) {
			Assert.assertTrue(check_data.get(i));
		}
	}

	// -------------------Marque filter----------------------------
	@When("^:I search with \"([^\"]*)\" less than three characters in marque filter in mes pharmacies it gives me an error message$")
	public void i_search_with_less_than_three_characters_in_marque_filter_in_mes_pharmacies_it_gives_me_an_error_message(
			String arg1) {
		Boolean ErrorMessageDisplayed = mes_pharmacies.verify_Marque_search_character_limits(arg1);
		Assert.assertTrue(ErrorMessageDisplayed);
	}

	@Then("^: Make sure suggested values related to marque filter \"([^\"]*)\" in Mes pharmacies$")
	public void make_sure_suggested_values_related_to_marque_filter_in_Mes_pharmacies(String arg1) {
		List<Boolean> suggested_values = mes_pharmacies.verify_Marque_filter_search_suggestedValues(arg1);
		for (int i = 0; i < suggested_values.size(); i++) {
			Assert.assertTrue(suggested_values.get(i));
		}
	}

	@Then("^: check data output based on \"([^\"]*)\" and \"([^\"]*)\" in marque filter mes pharmacies$")
	public void check_data_output_based_on_and_in_marque_filter_mes_pharmacies(String arg1, String arg2) {
		List<Boolean> check_data=mes_pharmacies.verify_Marque_filter_addedValues(arg1, arg2);
		for (int i = 0; i < check_data.size(); i++) {
			Assert.assertTrue(check_data.get(i));
		}
	}
	//-------------Nom du produit--------------------
	@When("^:I search with \"([^\"]*)\" less than three characters in Nom du produit filter in mes pharmacies it gives me an error message$")
	public void i_search_with_less_than_three_characters_in_Nom_du_produit_filter_in_mes_pharmacies_it_gives_me_an_error_message(String arg1){
		Boolean ErrorMessageDisplayed = mes_pharmacies.verify_NomDuProduit_search_character_limits(arg1);
		Assert.assertTrue(ErrorMessageDisplayed);
	}

	@Then("^: Make sure suggested values related to Nom du produit filter \"([^\"]*)\" in Mes pharmacies$")
	public void make_sure_suggested_values_related_to_Nom_du_produit_filter_in_Mes_pharmacies(String arg1) {
		List<Boolean> suggested_values = mes_pharmacies.verify_NomDuProduit_filter_search_suggestedValues(arg1);
		for (int i = 0; i < suggested_values.size(); i++) {
			Assert.assertTrue(suggested_values.get(i));
		}
	}

	@Then("^: check data output based on \"([^\"]*)\" and \"([^\"]*)\" in Nom du produit filter mes pharmacies$")
	public void check_data_output_based_on_and_in_Nom_du_produit_filter_mes_pharmacies(String arg1, String arg2) {
		List<Boolean> check_data=mes_pharmacies.verify_NomDuProduit_filter_addedValues(arg1, arg2);
		for (int i = 0; i < check_data.size(); i++) {
			Assert.assertTrue(check_data.get(i));
		}
	}
	
	//-------------Generic and principes-----------------
	@Then("^: select any value in Generic and Princepes filter in mes pharmacies tile and check data output$")
	public void select_any_value_in_Generic_and_Princepes_filter_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> GenericandPrincipes_check = mes_pharmacies.verify_GenericandPrincipes_filter_options();
		for (int i = 0; i < GenericandPrincipes_check.size(); i++) {
			Assert.assertTrue(GenericandPrincipes_check.get(i));
		}
	}
	
	//---------------Type de ventes--------------------------
	@Then("^: select any value in Type de ventes filter in mes pharmacies tile and check data output$")
	public void select_any_value_in_Type_de_ventes_filter_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> TypeDeVentes_check = mes_pharmacies.verify_TypeDeVentes_filter_options();
		for (int i = 0; i < TypeDeVentes_check.size(); i++) {
			Assert.assertTrue(TypeDeVentes_check.get(i));
		}
	}
	//----------Medicament OTC----------------------------
	@Then("^: select any value in Médicament OTC filter in mes pharmacies tile and check data output$")
	public void select_any_value_in_Médicament_OTC_filter_in_mes_pharmacies_tile_and_check_data_output() {
		List<Boolean> MedicamentOTC_check = mes_pharmacies.verify_MedicamentOTC_filter_options();
		for (int i = 0; i < MedicamentOTC_check.size(); i++) {
			Assert.assertTrue(MedicamentOTC_check.get(i));
		}
	}
}
