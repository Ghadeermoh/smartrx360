package com.Smartrx.Utility;

import com.Smartrx.Pages.BaseClass;

public class ObservatoireData extends BaseClass{
	
	public static String Expected_tile_name="Observatoire des prix";
	public static String[] Expected_observatoire_default_filters= {"5 Pharmacies","France (Données non extrapolées)"
	,"Top 40 des produits en TVA 20%","Dernier prix de vente","28 derniers jours","2B PHARMACIE"};
    public static String Expected_default_filter_color="rgba(0, 202, 255, 1)";
}
