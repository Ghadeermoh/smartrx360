package com.Smartrx.Utility;

import com.Smartrx.Pages.BaseClass;

public class MesPharmaciesData extends BaseClass {

	public static String tile_name = "Mes pharmacies";
	public static String groupment_name = "#Demo CegedimRx#";
	public static String number_of_pharmacies = "21";
	public static String[] column_header_default_xpath = { "Période de vente complète", "Quantité vendue",
			"% Évolution Quantité vendue", "CA TTC", "% Évolution CA TTC", "CA HT", "% Évolution CA HT",
			"Ajustée au regard d’anomalies sur le prix d'achat moyen pondéré", "% Évolution Marge Ajustée", "Marge LGO",
			"% Évolution Marge LGO", "(Prix Vente HT - Prix Achat HT) / Prix Achat HT",
			"(Prix Vente HT - Prix Achat HT) / Prix Vente HT", "# Ventes", "% Évolution # Ventes", "Panier TTC facture",
			"% Évolution Panier TTC facture", "Période d'achat complète", "Quantité commandée", "Quantité livrée",
			"Total Achats réalisés en prix remisés LGO", "Quantité en Stock", "Date dernière vente transmise " };
	
 public static String[] default_column_header= { "Période de vente complète", "Quantité vendue",
			"% Évolution Quantité vendue", "CA TTC", "% Évolution CA TTC", "CA HT", "% Évolution CA HT",
			"Marge Ajustée", "% Évolution Marge Ajustée", "Marge LGO",
			"% Évolution Marge LGO", "Taux Marge",
			"Taux Marque", "# Ventes", "% Évolution # Ventes", "Panier TTC facture",
			"% Évolution Panier TTC facture", "Période d'achat complète", "Quantité commandée", "Quantité livrée",
			"Total Achats réalisés en prix remisés LGO", "Quantité en Stock", "Date dernière vente transmise " };
		 
	public static String[] Expected_mespharmacies_default_filters = { "Résultat par pharmacies",
			"Tous les laboratoires", "Dernier mois complet", "Année précédente","Tout()"};

	//green , Orange , red 
	public static String[]Lastdate_transmission_colors= {"rgba(123, 212, 29, 1)","rgba(255, 152, 0, 1)","rgba(255, 23, 68, 1)"};
}
