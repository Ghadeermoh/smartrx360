package com.Smartrx.Utility;

import com.Smartrx.Pages.BaseClass;

public class ComparisonData extends BaseClass {
	public static String Expected_comparaison_title = "Comparaison";
	public static String Expected_default_filter_color = "rgba(0, 202, 255, 1)";
	public static String groumentName = "#Demo CegedimRx#";

	public static String[] Expected_Tables_analysis1_title = { "Ventes", "TVA", "Substitution en nombre de boîtes",
			"Famille" };

	public static String[] Expected_Tables_analysis2_title = {
			"Activité \"Période\" avec les paramètres de rémunération de 2020",
			"Activité \"Période\" avec les paramètres de rémunération de 2017",
			"Activité \"Période\" avec les paramètres de rémunération de 2020",
			"Activité \"Période\" avec les paramètres de rémunération de 2017",
			"Activité \"Période\" avec les paramètres de rémunération de 2020",
			"Activité \"Période\" avec les paramètres de rémunération de 2017" };
	public static String[] Expected_comparison_default_filters = { "Comparaison standard",
			"France (Données non extrapolées)", "Mensuel", "septembre 2020", "DUCHENES", "#Groupement Demo#" };

	public static String[] columns_analysis1 = { "", "Montant", "Ma pharmacie", "Écart avec le segment",
			"% évolution du segment", "Le segment", "", "Montant", "Ma pharmacie",
			"Écart avec le segment", "% évolution du segment", "Quantité vendue par segment", "", "Montant",
			"% répartition", "Ma pharmacie", "Écart avec le segment", "% évolution du segment",
			"Quantité vendue par segment", "", "Montant", "Ma pharmacie", "Écart avec le segment",
			"% évolution du segment", "Quantité vendue par segment" };

	public static String[] column_analysis2 = { "Médicaments", "CA HT par tranche", "Quantité vendue",
			"Marge théorique", "Écart Activité avec les paramètres 2019 Écart avec les paramètres 2017", "Médicaments",
			"CA HT par tranche", "Quantité vendue", "Marge théorique", "Médicaments", "CA HT", "Quantité vendue",
			"Marge théorique", "Écart Activité avec les paramètres 2019 Écart avec les paramètres 2017", "Médicaments",
			"CA HT", "Quantité vendue", "Marge théorique", "Total Médicaments et Honoraires", "Marge théorique",
			"Écart Activité avec les paramètres 2019 Écart avec les paramètres 2017", "Médicaments",
			"Marge théorique" };

	public static String[] Expected_Tables_analysis1_rows = { "Quantité vendue", "CA TTC", "Marge Ajustée",
			"Panier TTC facture", "# Ventes", "TVA 2,1 %", "TVA 5,5 %", "TVA 10 %", "TVA 20 %", "Exonéré", "Générique",
			"Princeps", "Spécialités", "Dermocosmétique", "Accessoires et orthopédie", "Diététique", "Homéopathie",
			"Phytothérapie", "Divers", "Vétérinaire" };
	public static String[] Expected_Tables_analysis2_rows = { "Tranche 1: 0,0 € à 1,91 €",
			"Tranche 2: 1,92 € et 22,90 €", "Tranche 3: 22,91 € et 150,00 €", "Tranche 4: 150,01 € et 1930,00 €",
			"Tranche supérieure à 1930,01 €", "Médicaments Génériques", "TOTAL", "Tranche 1: 0,0 € à 1,91 €",
			"Tranche 2: 1,92 € et 22,90 €", "Tranche 3: 22,91 € et 150,00 €", "Tranche 4: 150,01 € et 1500,00 €",
			"Tranche supérieure à 1500,01 €", "Médicaments Génériques", "TOTAL", "Honoraires petit conditionnement",
			"Honoraires conditionnement trimestriel", "Honoraires complexes", "Honoraires liés a l'age du bénéficiaire",
			"" };
	public static String Expected_default_buttons_color = "rgba(38, 55, 86, 1)";

}
