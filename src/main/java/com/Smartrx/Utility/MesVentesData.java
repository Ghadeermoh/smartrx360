package com.Smartrx.Utility;

public class MesVentesData {

	public static String Expected_mesventes_title = "Mes ventes groupement";
	public static String Expected_mesventes_pharmacies = "35";
	public static String Expected_mesventes_groupment = "#Groupement Demo#";
	public static String[] Expected_mesventes_default_filters = { "France (Données non extrapolées)",
			"Résultats par famille BCB (Niveau 1)", "Tous les laboratoires", "Dernier mois complet", "Année précédente",
			"#Groupement Demo FR#" };

	public static String[] default_table_header = { "Quantité vendue", "% Évolution Quantité vendue", "CA TTC",
			"% Évolution CA TTC", "CA HT", "% Évolution CA HT", "Marge Ajustée", "% Évolution Marge Ajustée",
			"Marge LGO", "% Évolution Marge LGO", "Taux Marge", "Taux Marque", "# Ventes", "% Évolution # Ventes",
			"Panier TTC facture", "% Évolution Panier TTC facture", "Panier TTC produits (Moyenne)",
			"% Évolution Panier TTC produits", "Quantité en Stock" };

	public static String[] comparisonSwitch_table_header = { "Quantité vendue", "% Évolution Quantité vendue",
			"Écart avec le segment", "CA TTC", "% Évolution CA TTC", "Écart avec le segment", "CA HT",
			"% Évolution CA HT", "Écart avec le segment", "Marge Ajustée", "% Évolution Marge Ajustée",
			"Écart avec le segment", "# Ventes", "% Évolution # Ventes", "Écart avec le segment", "Panier TTC facture",
			"% Évolution Panier TTC facture", "Écart avec le segment" };

	public static String[] Expected_mesventes_analysis_column = { "Famille BCB (niveau 1)", "Famille BCB (niveau 2)",
			"Taux TVA", "Indications thérapeutiques", "Segmentation (niveau 1)", "Générique, princeps & autres",
			"Groupe Générique", "Laboratoire Exploitant", "Marque", "Identifiant BCB", "Type de produit", "Mois",
			"Type de vente", "Age", "Pharmacies", "Pharmacies", "Type d'honoraires" };

	public static String[] Expected_mesventes_analysis_chart = { "Famille BCB (Niveau 1)", "Famille BCB (Niveau 2)",
			"Taux TVA", "Indications thérapeutiques", "Segmentation (Niveau 1)", "Générique, princeps & autres",
			"Groupe Générique", "Laboratoire Exploitant", "Marque", "Produit", "Type de produit", "Mois",
			"Type de vente", "Age", "Nom Opérateur", "Pharmacies", "Type d'honoraires" };

	public static String[] Expected_mesventes_partdemarche = { "  Part de marché Quantité vendue",
			"  Part de marché CA TTC", "  Part de marché CA HT", "  Part de marché Marge Ajustée",
			"  Part de marché Marge LGO", "  Part de marché # Ventes", "  Part de marché Panier TTC produits" };
//"  Part de marché Panier TTC facture"   ---removed in jira 861

	public static String[] analysis_Type_options = { "Résultats par famille BCB (Niveau 1)",
			"Résultats par famille BCB (Niveau 2)", "Résultats par taux de TVA",
			"Résultats par indication thérapeutique", "Résultats par segmentation (Niveau 1)", "Résultats par princeps",
			"Résultats par groupe générique", "Résultats par laboratoire exploitant (Top 1000)",
			"Résultats par marque (Top 1000)", "Résultats par produit (Top 1000)", "Résultats par type de produit",
			"Résultats par mois", "Résultats par type de vente", "Résultats par age du client",
			"Résultats par opérateur (Top 1000)", "Résultats par pharmacie", "Résultats par type d'honoraires" };

	public static String[] period_options = { "Aujourd'hui", "Hier", "7 derniers jours (J-1)",
			"28 derniers jours (J-1)", "Dernier mois complet", "3 derniers mois (M-1)", "12 derniers mois (M-1)",
			"24 derniers mois (M-1)", "Depuis le début de l'année (J-1)", "Dernière année calendaire complète",
			"Date personnalisée" };

	public static String[] FamilleBCB1 = { "Spécialités", "Accessoires et orthopédie", "Non Renseigné", "Homéopathie",
			"Dermocosmétique", "Diététique", "Divers","Phytothérapie","Vétérinaire" };
}
