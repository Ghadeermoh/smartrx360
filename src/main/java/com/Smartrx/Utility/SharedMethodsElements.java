package com.Smartrx.Utility;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.Smartrx.Pages.BaseClass;
import com.Smartrx.Pages.MesVentesGroupment;

public class SharedMethodsElements extends BaseClass {

	public static void table_wait() {
		By loadingProgress = By.xpath("div[@ng-show=\"chart.loading\"]");
		UtilityClass.waitExpectedConditionsElementContainsAttributeText(loadingProgress, "aria-hidden", "true");
	}

	public static List<Boolean> check_filter_options(WebElement filter, String filter_options_xpath,
			WebElement used_filters_name, WebElement first_chart_data, WebElement second_chart_data ,String table_output) {
		filter.click();
		List<WebElement> filter_options = driver.findElements(By.xpath(filter_options_xpath));
		List<Boolean> check = new ArrayList<>();
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println(filter_options.get(i).getText());
			filter_options.get(i).click();
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			if (filter_options.get(i).getText().contentEquals("Date personnalisée")) {
				break;
			}
//			MesVentesGroupment.verify_table_columns();
			boolean filter_name_check = (used_filters_name.getText().contains(filter_options.get(i).getText()));
			boolean chart1_check = (first_chart_data.getText().contains(filter_options.get(i).getText()));
			boolean chart2_check = (second_chart_data.getText().contains(filter_options.get(i).getText()));
			check.add(filter_name_check);
			check.add(chart1_check);
			check.add(chart2_check);
			List<WebElement>table_data=driver.findElements(By.xpath(table_output));
			for(int j=0;j<table_data.size()-1;j++) {
				Boolean dataoutput_check=table_data.get(i).getText().contentEquals(MesVentesData.FamilleBCB1[j]);
				check.add(dataoutput_check);
			}
		}
		return check;
	}

	public static List<Boolean> check_filter_options(WebElement filter, String filter_options_xpath,
			WebElement data_output, String type) {
		filter.click();
		List<WebElement> filter_options = driver.findElements(By.xpath(filter_options_xpath));
		List<Boolean> check = new ArrayList<>();
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println(filter_options.get(i).getText());
			filter_options.get(i).click();
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			if (filter_options.get(i).getText().contentEquals("Date personnalisée")) {
				break;
			}
			if (type == "2") {
				System.out.println("xxxxxx----------xxxxxxxx");
			} else if (type == "1") {
//				MesVentesGroupment.verify_table_columns();
			}
			System.out.println("filter name ---"+data_output.getText());
			boolean filter_name_check = (data_output.getText().contains(filter_options.get(i).getText()));
			check.add(filter_name_check);
		}
		return check;
	}

	public static List<Boolean> check_filterClassification_options(WebElement filter, String filter_options_xpath,
			String data_output, String check_box, String wait_xpath) {
		filter.click();
		List<Boolean> check_data = new ArrayList<>();
		List<WebElement> filter_options = driver.findElements(By.xpath(filter_options_xpath));
		List<WebElement> checkBox_options = driver.findElements(By.xpath(check_box));
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println(filter_options.size());
			String text = filter_options.get(i).getText();
			System.out.println("filter options---" + text);
			String[] text_all = text.split(":");
			text_all[1] = text_all[1].replaceAll("[0-9]", "");
			text_all[1] = text_all[1].substring(0, text_all[1].length() - 3);
			text_all[1] = text_all[1].trim();
			text_all[1] = text_all[1].strip();
			System.out.println("filter options---" + text_all[1]);
			UtilityClass.wait_element_toBeClickabke(check_box);
			UtilityClass.set_javaSc_executor(checkBox_options.get(i));
			String locator = "//div[contains(text(),'" + text_all[1] + "')]";
			UtilityClass.wait_element_invisibility(wait_xpath);
			UtilityClass.wait_element_visibility(locator);
			String data = driver.findElement(By.xpath(data_output)).getText();
			Boolean check = filter_options.get(i).getText().contains(data);
			System.out.println(check);
			check_data.add(check);
			UtilityClass.set_javaSc_executor(checkBox_options.get(i));
			UtilityClass.wait_element_invisibility(wait_xpath);
		}
		return check_data;
	}

	public static List<Boolean> check_filter_options(WebElement filter, String filter_options_xpath, String data_output,
			String close_xpath, String wait_xpath) {
		filter.click();
		UtilityClass.wait_element_visibility(wait_xpath);
		List<WebElement> filter_options = driver.findElements(By.xpath(filter_options_xpath));
		System.out.println(filter_options.size());
		List<Boolean> check = new ArrayList<>();
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println(filter_options.get(i).getText());
			UtilityClass.set_javaSc_executor(filter_options.get(i));
			String data_wait = "//div[contains(text(),'" + filter_options.get(i).getText() + "')]";
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
			UtilityClass.wait_element_visibility(data_wait);
			WebElement data = driver.findElement(By.xpath(data_output));
			boolean data_check = (data.getText().contentEquals(filter_options.get(i).getText()));
			check.add(data_check);
			WebElement close_x=driver.findElement(By.xpath(close_xpath));
			UtilityClass.set_javaSc_executor(close_x);
			UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
		}
		return check;
	}

	public static List<Boolean> check_filter_search_suggested_values(WebElement filter, WebElement search_box,
			String Suggested_values_xpath, String search_val) {
		filter.click();
		search_box.clear();
		search_box.sendKeys(search_val);
		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])[1]");
		table_wait();
		List<WebElement> suggested_val = driver.findElements(By.xpath(Suggested_values_xpath));
		List<Boolean> Check_sugg_valus = new ArrayList<>();
		Boolean Check_suggested_values = suggested_val.size() > 0;
		Check_sugg_valus.add(Check_suggested_values);
		for (int i = 0; i < suggested_val.size(); i++) {
			System.out.println("suggested values ---" + suggested_val.get(i).getText());
			Boolean check_suggested_with_search = (suggested_val.get(i).getText().contains(search_val));
			Check_sugg_valus.add(check_suggested_with_search);
		}
		return Check_sugg_valus;
	}

	public static List<Boolean> check_dataoutput_withSelectedValue(String Suggested_values_xpath,
			String data_output_xpath, String search_val, String selected_val) {
		List<WebElement> suggested_val = driver.findElements(By.xpath(Suggested_values_xpath));
		List<Boolean> check_dataOutput = new ArrayList<>();
		int flag = 0;
		for (int i = 0; i < suggested_val.size(); i++) {
			System.out.println("selected value " + selected_val);
			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
				suggested_val.get(i).click();
				UtilityClass.wait_element_visibility(data_output_xpath);
				String data_row = driver.findElement(By.xpath(data_output_xpath)).getText();
				System.out.println("data_output" + data_row);
				Boolean check_data = data_row.contentEquals(selected_val);
				check_dataOutput.add(check_data);
				flag++;
			}
		}
		if (flag == 0) {
			System.out.println("------Value Not exist-----------");
		}
		return check_dataOutput;
	}

	public static List<Boolean> check_dataoutput_withSelectedValue(String Suggested_values_xpath,
			String data_output_xpath, String search_val, String selected_val, WebElement Ajouter_button) {
		List<WebElement> suggested_val = driver.findElements(By.xpath(Suggested_values_xpath));
		List<Boolean> check_dataOutput = new ArrayList<>();
		int flag = 0;
		for (int i = 0; i < suggested_val.size(); i++) {
			System.out.println("selected value " + selected_val);
			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
				suggested_val.get(i).click();
				Ajouter_button.click();
				table_wait();
				List<WebElement> data_elements = driver.findElements(By.xpath(data_output_xpath));
				Boolean data_size_check = (data_elements.size() > 0);
				check_dataOutput.add(data_size_check);
				for (int j = 0; j < data_elements.size() - 1; j++) {
					System.out.println("data_output" + data_elements.get(j).getText());
					Boolean check_data = data_elements.get(j).getText().contains(selected_val);
					check_dataOutput.add(check_data);
				}
				flag++;
			}
		}
		if (flag == 0) {
			System.out.println("------Value Not exist-----------");
		}
		return check_dataOutput;
	}

	public static List<Boolean> check_dataoutput_withSelectedValueLaboratoire(String Suggested_values_xpath,
			String data_output_xpath, String search_val, String selected_val, WebElement Ajouter_button) {
		List<WebElement> suggested_val = driver.findElements(By.xpath(Suggested_values_xpath));
		List<Boolean> check_dataOutput = new ArrayList<>();
		int flag = 0;
		driver.findElement(By.xpath("//div[contains(text(),'Tout sélectionner')]")).click();
		driver.findElement(By.xpath("//div[contains(text(),'Laboratoire Exploitant')]")).click();
		for (int i = 0; i < suggested_val.size(); i++) {
			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
				suggested_val.get(i).click();
				Ajouter_button.click();
				table_wait();
				UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
				String Laboratoire = driver.findElement(By.xpath(data_output_xpath)).getText();
				System.out.println("data_output----" + Laboratoire);
				Boolean check_data = Laboratoire.contains(selected_val);
				check_dataOutput.add(check_data);
				flag++;
			}
		}
		if (flag == 0) {
			System.out.println("------Value Not exist-----------");
		}
		return check_dataOutput;
	}

	public static Boolean verify_filter_search_character_limits(String search_val, WebElement filter,
			WebElement more_filterLink, WebElement search_box) {
		more_filterLink.click();
		filter.click();
		search_box.clear();
		search_box.sendKeys(search_val);
		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])[1]");
		Boolean error_message_isDisplayed = driver
				.findElement(
						By.xpath("//span[contains(text(),'Saisissez au moins 3 caractères pour lancer la recherche')]"))
				.isDisplayed();
		return error_message_isDisplayed;
	}

	public static void date_personalisee(WebElement filter, String date_box1, String date_box2, String date_debut,
			String date_fin) {
		filter.click();
		driver.findElement(By.xpath("//div[contains(text(),'Date personnalisée')]")).click();
		driver.findElement(By.xpath(date_box1)).sendKeys(date_debut);
		driver.findElement(By.xpath(date_box2)).sendKeys(date_fin);
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
//		MesVentesGroupment.verify_table_columns();
	}

	public static Boolean verify_OuiAndNon_filter(WebElement filter, WebElement more_filter, String options_xpath,
			String data_output_xpath, String expceted_data) {
		more_filter.click();
		filter.click();
		List<WebElement> filter_options = driver.findElements(By.xpath(options_xpath));
		Boolean check_data = false;
		for (int i = 0; i < filter_options.size(); i++) {
			filter_options.get(i).click();
			WebElement data_elements = driver.findElement(By.xpath(data_output_xpath));
			if (filter_options.get(i).getText().contentEquals("Oui")) {
				check_data = (data_elements.getText().contains(expceted_data));
			}
			driver.findElement(By.xpath("//span[@class='cg-font cg-font-times-circle']")).click();
		}
		return check_data;

	}

	public static List<Boolean> Check_filterOptions_duplication(WebElement filter, String filter_options_xpath) {
		filter.click();
		List<WebElement> filter_options = driver.findElements(By.xpath(filter_options_xpath));
		List<Boolean> duplication_check = new ArrayList<Boolean>();
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println("first element--- " + filter_options.get(i).getText());
			for (int j = i + 1; j < filter_options.size(); j++) {
				System.out.println("elements====" + filter_options.get(j).getText());
				Boolean check = (!filter_options.get(i).getText().equals(filter_options.get(j).getText()));
				duplication_check.add(check);
			}
		}
		return duplication_check;
	}

}
