package com.Smartrx.Utility;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Smartrx.Pages.BaseClass;

public class UtilityClass extends BaseClass {

	// location of used files
	public static String config_path = "C:\\Users\\gmohamed\\Smartrx360Group\\src\\main\\java\\com\\Smartrx\\SysConfig\\system.properties";
	public static String chrome_path = "D:\\selenium programs\\chromedriver_win32\\chromedriver.exe";
	public static String firefox_path = "D:\\selenium programs\\geckodriver.exe";
	// =======================================================================
	// wait
	public static int Page_Load_TimeOut = 200;
	public static int Implicit_Wait = 40;

	public static void set_javaSc_executor(WebElement elem) {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", elem);
	}

	public static boolean waitExpectedConditionsElementContainsAttributeText(By by, String atterbute, String text) {
		WebDriverWait wait = new WebDriverWait(driver, 30 /* seconds */);
		List<WebElement> elements = driver.findElements(by);
		// wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
		elements.forEach(element -> wait.until(ExpectedConditions.attributeContains(element, "aria-hidden", "true")));
		return elements.stream().allMatch(element -> element.getAttribute(atterbute).contains(text));
	}

	public static void wait_element_visibility(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
	}

	public static void wait_element_invisibility(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
	}

	public static void wait_element_toBeClickabke(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
	}

	// =============================================================
	public static String scroll(String click_element, String xpath_value) throws Exception {
		String element_text = "";
		WebDriverWait wb = new WebDriverWait(driver, 1);
		Actions dragger = new Actions(driver);
		WebElement row = driver.findElement(By.xpath(click_element));
		int scrollPoints = 5000;
		try {
			System.out.println("---------------- Started - scroll_Page ----------------");
			dragger.doubleClick(row);
			int numberOfPixelsToDragTheScrollbarDown = 20;
			for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown) {
				dragger.sendKeys(Keys.ARROW_RIGHT).build().perform();
				try {
					if (wb.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath_value))) != null
							&& wb.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath_value))) != null) {
						System.out.println("Got element!!");
						element_text = (driver.findElement(By.xpath(xpath_value)).getText());
						Thread.sleep(10);
						System.out.println("---------------- Ending - scroll_Page ----------------");
						break;
					}
				} catch (Exception e) {
					element_text = "not found";
					System.out.println("Element not found. Scrolling again");
				}
			}
		} catch (Exception ex) {
			System.out.println("---------------- scroll is unsucessfully done in scroll_Page ----------------");
			ex.printStackTrace();
		}
		return element_text;
	}

	// ===============================================================================================

	public static void scroll_down(String scroll_xpath, int Scroll_pixels) {
		Actions dragger = new Actions(driver);
		WebElement draggablePartOfScrollbar = driver.findElement(By.xpath(scroll_xpath));
		int numberOfPixelsToDragTheScrollbarDown = Scroll_pixels;
		dragger.moveToElement(draggablePartOfScrollbar).clickAndHold()
				.moveByOffset(0, numberOfPixelsToDragTheScrollbarDown).release().perform();

	}

	public static void scrolltry(String xpath) {
		EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
		eventFiringWebDriver.executeScript(
				"document.querySelector('(//div[@class=\"md-virtual-repeat-scroller\"][@role=\"presentation\"])[3]').scrollTop=150");
	}

	// ========================================================================
	public static void scroll_intoview(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	// -----------------------------------------------------------------------------
	public static void check_downloadedFile_size(int x) {
		File listoffiles[] = folder.listFiles();
		// make sure directory is not empty
		Assert.assertTrue(listoffiles.length > x);
		System.out.println(listoffiles.length);
		for (File file : listoffiles) {
			// make sure downloaded file is not empty
			Assert.assertTrue(file.length() > 0);
		}
	}

	public static long find_difference_betweenTwoDates(String first_date , String second_date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
	    Date firstDate = sdf.parse(first_date);
	    Date secondDate = sdf.parse(second_date);
	    long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
	    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
	    System.out.println(diff);
	    return diff;
	}

}
