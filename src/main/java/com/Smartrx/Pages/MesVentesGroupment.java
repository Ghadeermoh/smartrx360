package com.Smartrx.Pages;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.Smartrx.Utility.ComparisonData;
import com.Smartrx.Utility.MesVentesData;
import com.Smartrx.Utility.SharedMethodsElements;
import com.Smartrx.Utility.UtilityClass;

public class MesVentesGroupment extends BaseClass {

	@FindBy(xpath = "//div[contains(text(),'Mes ventes groupement')]")
	WebElement MesVentesGroupmentLink;

	@FindBy(xpath = "//span[contains(text(),'Mes ventes groupement')]")
	WebElement MesVentesTitle;

	@FindBy(id = "HELP_SUBHEADER_LAYOUT")
	WebElement groupment_name;

	@FindBy(xpath = "(//span//b[@class='ng-binding'])[2]")
	WebElement numberOfPharmacies;

	@FindBy(xpath = "//div[@id=\"HELP_SIDENAV_COMPARISON\"]// md-switch //div[@class=\"md-container\"]")
	static WebElement comparison_switch;

	@FindBy(xpath = "//div//span[contains(text(),'Voir plus de filtres')]")
	WebElement more_filter;

	@FindBy(xpath = "(//button[@id='HELP_menuIcon_chart2_GROUP_SALES'])")
	WebElement BurgerTableIcon;

	@FindBy(xpath = "//button[@id=\"GRID_EXPORT_CSV\"]")
	WebElement csv_downloadIcon;

	@FindBy(id = "GRID_EXPORT_PDF")
	WebElement pdf_downloadIcon;

	@FindBy(id = "GRID_OPEN_MENU")
	WebElement settings;

	@FindBy(xpath = "//md-card[@id='HELP_chart2_GROUP_SALES']//span[1]")
	WebElement used_filters_name;

	@FindBy(xpath = "//div[contains(text(),'vs')]")
	WebElement comparison_usedFilter_name;

	// -------charts elements-----------------------
	@FindBy(xpath = "(//md-card[@id='HELP_chart0_GROUP_SALES']//span)[1]")
	WebElement first_chart_data;

	@FindBy(xpath = "(//md-card[@id='HELP_chart1_GROUP_SALES']//span)[1]")
	WebElement second_chart_data;

	// -------filters-----------
	@FindBy(id = "TYPE_SELECT")
	WebElement comparison_segment;

	@FindBy(id = "STR_MARKET_DISPLAY_OPTIONS")
	WebElement analysis_type;

	@FindBy(xpath = "(//div[@class='cg-background-color-alt-2 cg-hover layout-align-start-center layout-row flex'])[4]")
	WebElement favorite_labs;

	@FindBy(id = "HELP_PERIOD_FILTER")
	WebElement period;

	@FindBy(id = "HELP_EVOL_FILTER")
	WebElement evolution;

	@FindBy(id = "MARKET_SELECT")
	WebElement marche;

	@FindBy(id = "HELP_tenant_group")
	WebElement groupments;

	@FindBy(id = "HELP_tenant")
	WebElement pharmacies;

	@FindBy(id = "HELP_lines_vatRateFixed")
	WebElement TauxTVA;

	@FindBy(id = "HELP_lines_product_bcbInfos_bcbFamilyCode")
	WebElement FamileBCB;

	@FindBy(id = "HELP_lines_product_bcbInfos_atcClassCode")
	WebElement ClassATC;

	@FindBy(xpath = "//div[@id='HELP_DATASTATE_FILTER']")
	WebElement Etat_de_donnees;

	@FindBy(xpath = "//span[contains(text(),'Laboratoire BCB')]")
	WebElement Laboratoire_BCB;

	@FindBy(id = "HELP_lines_product_bcbInfos_brandName")
	WebElement Marque;

	@FindBy(id = "HELP_lines_product_bcbInfos_libelle")
	WebElement NomDuProduit;

	@FindBy(id = "HELP_lines_product_bcbInfos_genericPrinceps")
	WebElement Genericandprincepes;

	@FindBy(id = "HELP_isPrescription")
	WebElement typeDeVentes;

	@FindBy(xpath = "//div[@id='HELP_lines_product_bcbInfos_genericGroupCodeLabel']")
	WebElement GroupeGeneric;

	@FindBy(id = "HELP_lines_product_bcbInfos_isOtc")
	WebElement MedicamentOTC;

	@FindBy(id = "HELP_lines_product_bcbInfos_isExpensive")
	WebElement ProduitCher;

	@FindBy(id = "HELP_lines_product_bcbInfos_segmentationCode")
	WebElement Segmentation;

	@FindBy(id = "HELP_lines_product_bcbInfos_indicationCIM10Code")
	WebElement Indications_thérapeutiques;

	// -----------------search fields inside filters-------------------------
	@FindBy(xpath = "(//input[@type='search'])[3]")
	WebElement analysis_search_input;

	@FindBy(xpath = "(//input[@type='search'])[5]")
	WebElement pharmacies_search_input;

	@FindBy(xpath = "(//input[@type='search'])[5]")
	WebElement marche_search_input;

	@FindBy(xpath = "(//input[@type='search'])[4]")
	WebElement favorite_labs_search_input;

	@FindBy(xpath = "(//input[@type='search'])[7]")
	WebElement Marque_search_input;

	@FindBy(xpath = "(//input[@type='search'])[7]")
	WebElement NomDeproduit_search_input;

	@FindBy(xpath = "(//input[@type='search'])[7]")
	WebElement Laboratoire_BCB_search_input;

	@FindBy(xpath = "(//input[@type='search'])[7]")
	WebElement NomDuProduit_search_input;

	@FindBy(xpath = "(//input[@type='search'])[7]")
	WebElement Indication_search_input;

	@FindBy(xpath = "(//input[@type='search'])[7]")
	WebElement GroupeGeneric_search_box;

	@FindBy(xpath = "//input[@class='ng-pristine ng-valid md-input ng-empty ng-touched']")
	WebElement marque_search_input;

	@FindBy(xpath = "(//button[@class='md-primary md-raised md-button md-ink-ripple'])[2]")
	WebElement AJOUTER_UN_FILTRE_Btn;

	// -------saved filters elements-----------------
	@FindBy(xpath = "//button[@class='md-primary md-raised md-button md-ink-ripple']")
	WebElement create_savedFilter_Btn;

	@FindBy(xpath = "//div[@class='cg-background-color-alt-2 cg-hover layout-align-start-center layout-row flex']//span[contains(text(),'Mes filtres sauvegardés ')]")
	WebElement mysaved_filters;

	@FindBy(xpath = "(//input[@name='editItemName'])[1]")
	WebElement saved_filter_name;

	@FindBy(xpath = "(//input[@name='editItemName'])[2]")
	WebElement saved_filter_desc;

	@FindBy(xpath = "//button[contains(text(),'Sauver')]")
	WebElement saved_filter_saveBtn;

	@FindBy(xpath = "//div[@class='ng-scope layout-row']//div[@class='cg-ellipsis cg-hover ng-binding']")
	WebElement created_saved_filter;

	@FindBy(xpath = "//span[@class='cg-font cg-hover cg-font-star-o']")
	WebElement savedFilter_Star;

	@FindBy(xpath = "//span[@class='cg-font cg-font-trash cg-hover']")
	WebElement delete_saved_filter;

	@FindBy(xpath = "(//md-select[@name='editItemName'])")
	WebElement filterType_selection;

	public MesVentesGroupment() {
		PageFactory.initElements(driver, this);
	}

	public void table_wait() {
		By loadingProgress = By.xpath("div[@ng-show=\"chart.loading\"]");
		UtilityClass.waitExpectedConditionsElementContainsAttributeText(loadingProgress, "aria-hidden", "true");
	}

	// --------------------------------------------background---------------------------
	public void Open_mes_ventes_groupment_Tile() {
		UtilityClass.set_javaSc_executor(MesVentesGroupmentLink);
	}

	// --------------------------------------------Scenario1-----------------------------
	public String verify_tile_title() {
		return MesVentesTitle.getText();
	}

	public String verify_numer_of_pharmacies() {
		return numberOfPharmacies.getText();
	}

	public String verify_groupment_name() {
		return groupment_name.getText();
	}

	// --------------------------------------------Scenario2--------------------------------
	public void verify_default_filters_selection() {
		comparison_switch.click();
		favorite_labs.click();
		comparison_segment.click();
		analysis_type.click();
		period.click();
		evolution.click();
		groupments.click();
		List<WebElement> default_filters = driver
				.findElements(By.xpath("//div[contains(@class,'cg-sidenav-text-selected')]"));
		System.out.println(default_filters.size());
		for (int i = 0; i < default_filters.size(); i++) {
			System.out.println(default_filters.get(i).getText());
			Assert.assertEquals(default_filters.get(i).getText(), MesVentesData.Expected_mesventes_default_filters[i]);
			Assert.assertEquals(default_filters.get(i).getCssValue("color"),
					ComparisonData.Expected_default_filter_color);
		}
	}

	public static void verify_table_columns() {
		for (int i = 0; i < MesVentesData.default_table_header.length; i++) {
			String column_xpath = "//span[contains(text(),'" + MesVentesData.default_table_header[i] + "')]";
			System.out.println(column_xpath);
			String click_element = "(//div[@class='ui-grid-cell-contents cg-ellipsis ng-binding'])[" + (i + 10) + "]";
			System.out.println(click_element);
			try {
				String element = UtilityClass.scroll(click_element, column_xpath);
				System.out.println(element);
				Assert.assertEquals(MesVentesData.default_table_header[i], element);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// =================================================================
	public static void verify_table_columns_comparisonSwitch() {
		comparison_switch.click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
		for (int i = 0; i < MesVentesData.comparisonSwitch_table_header.length; i++) {
			String column_xpath = "//span[contains(text(),'" + MesVentesData.comparisonSwitch_table_header[i] + "')]";
			System.out.println(column_xpath);
			String click_element = "(//div[@class='ui-grid-cell-contents cg-ellipsis ng-binding'])[" + (i + 10) + "]";
			System.out.println(click_element);
			try {
				String element = UtilityClass.scroll(click_element, column_xpath);
				System.out.println(element);
				Assert.assertEquals(MesVentesData.comparisonSwitch_table_header[i], element);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// --------------------------------------------Scenario3--------------------------------------------
	public void verify_analysis_type_filter() {
		analysis_type.click();
		String newfirstElement_xpath = "//div[contains(text(),'Résultats par')][1]";
		String visibleList_xpath = "//div[contains(text(),'Résultats par')]";
		String scroll_xpath = "(//div[@class='md-virtual-repeat-scroller'][@role='presentation'])[3]//div";
		filter_container(newfirstElement_xpath, visibleList_xpath, scroll_xpath, 100, "analysis");
	}

//----------------------------------------------Scenario4-------------------------------------------------
	public List<Boolean> verify_period_filter_options() {
		String filter_options = "//div[@id='HELP_PERIOD_FILTER']//div[contains(@class,'cg-ellipsis ng-binding')]";
		String table_output_xpath="//div[@data-code='product_family_code']";
		List<Boolean> filter_options_check = SharedMethodsElements.check_filter_options(period, filter_options,
				used_filters_name, first_chart_data, second_chart_data ,table_output_xpath);
		return filter_options_check;
	}

	public void verify_specific_period_selection(String date_debut, String date_fin) {
		String date_box1 = "(//input[@class='md-datepicker-input'])[1]";
		String date_box2 = "(//input[@class='md-datepicker-input'])[2]";
		SharedMethodsElements.date_personalisee(period, date_box1, date_box2, date_debut, date_fin);
	}

	// -------------------------------------Scenario5------------------------------------------
	public List<Boolean> verify_evolution_filter_options() {
		String filter_options = "//div[@id='HELP_EVOL_FILTER']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> filter_options_check = SharedMethodsElements.check_filter_options(evolution, filter_options,
				used_filters_name, "1");
		return filter_options_check;
	}

	public void verify_specific_evolution_selection(String date_debut, String date_fin) {
		String date_box1 = "(//input[@class='md-datepicker-input'])[3]";
		String date_box2 = "(//input[@class='md-datepicker-input'])[4]";
		SharedMethodsElements.date_personalisee(evolution, date_box1, date_box2, date_debut, date_fin);
	}

//-------------------------------------------------Scenario6--------------------------------	
	public void verify_marche_filter_options() {
		table_wait();
		marche.click();
		List<String> marche_filters_options = filter_container(
				"//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')][1]",
				"//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]",
				"(//div[@class='md-virtual-repeat-scroller'][@role='presentation'])[5]//div", 7, "marche");
		for (int i = 0; i < marche_filters_options.size(); i++) {
			System.out.println(marche_filters_options.get(i));
		}
	}

//------------------------------------------Scenario7--------------------
	public int verify_pharmacies_filter_options() {
		pharmacies.click();
		String first_xpath = "(//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')])[1]";
		String visible_listxpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String scroll_xpath = "(//div[@class='md-virtual-repeat-scroller'][@role='presentation'])[5]//div";
		List<String> pharmacies_filters_options = filter_container(first_xpath, visible_listxpath, scroll_xpath, 117,
				"pharmacies");
		System.out.println(pharmacies_filters_options.size());
		for (int i = 0; i < pharmacies_filters_options.size(); i++) {
			System.out.println(pharmacies_filters_options.get(i));
		}
		return pharmacies_filters_options.size();
	}

	// ----------------------------------------------Scenario8------------------------
	public List<Boolean> verify_comparison_segment_filter_options() {
		comparison_switch.click();
		String filter_options_xpath = "//div[@id='TYPE_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> comparisonSegment_check = SharedMethodsElements.check_filter_options(comparison_segment,
				filter_options_xpath, comparison_usedFilter_name, "2");
		return comparisonSegment_check;
	}

	// ------------------------------------------Scenario9--------------------
	public List<Boolean> verify_groupment_filter_options() {
		groupments.click();
		List<WebElement> groupment_options = driver.findElements(
				By.xpath("//div[@id='HELP_tenant_group']//div[contains(@class,'cg-hover ng-scope layout-row')]"));
		List<Boolean> check_groupmentData = new ArrayList<>();
		for (int i = 0; i < groupment_options.size(); i++) {
			System.out.println(groupment_options.get(i).getText());
			groupment_options.get(i).click();
			UtilityClass.wait_element_visibility("//div[@data-code=\"PHARMA\"]");
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			List<WebElement> data_output = driver.findElements(By.xpath("//div[@data-code=\"PHARMA\"]"));
			int numOfPhar = Integer.parseInt(numberOfPharmacies.getText());
			Boolean check = data_output.size() == numOfPhar;
			check_groupmentData.add(check);
		}
		return check_groupmentData;
	}

	// ----------------------------------------Scenario10------------------
	public void verify_mesventes_downloadCSV() {
		csv_downloadIcon.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UtilityClass.check_downloadedFile_size(0);
	}

	public void verify_mesventes_downloadPDF() {
		pdf_downloadIcon.click();
	}

	// -----------------------@MVG_ECSV_03---------------------------
	public void verify_download_CSV_withDetails(String Details) {
		WebElement element = driver.findElement(By.xpath("//button[@id='HELP_menuIcon_chart2_GROUP_SALES']"));
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='HELP_menuIcon_chart2_GROUP_SALES']")));
		Actions action = new Actions(driver);
		action.moveToElement(element).click().perform();
		UtilityClass
				.set_javaSc_executor(driver.findElement(By.xpath("//button[@id='HELP_menuIcon_chart2_GROUP_SALES']")));
		UtilityClass.wait_element_visibility("//p[contains(text(),'Exporter avec Détails')]");
		UtilityClass.set_javaSc_executor(driver.findElement(By.xpath("//p[contains(text(),'Exporter avec Détails')]")));
		String detail_xpath = "(//button//div//p[contains(text(),'" + Details + "')])[10]";
		UtilityClass.set_javaSc_executor(driver.findElement(By.xpath(detail_xpath)));
		Assert.assertTrue(driver.findElement(By.xpath("//span[contains(text(),'Export en cours… Veuillez patienter')]"))
				.isDisplayed());
		UtilityClass.wait_element_visibility("//span[contains(text(),'Votre export du')]");
		driver.findElement(By.xpath("//span[@class='cg-hover ng-scope']")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UtilityClass.check_downloadedFile_size(0);
	}

	// -----------------------------------------------Scenario12-----------------
	public void select_analysisType_withTauxTVA() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par taux de TVA')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_TauxTVA_filter_options() {
		more_filter.click();
		String filter_options_xpath = "//div[@id='HELP_lines_vatRateFixed']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String TVA_row = "(//div[@class='cg-align-vertical-cell ng-scope'])[2]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		String wait_xpath = "//span[contains(text(),'TVA 2,1 %')]";
		List<Boolean> check = SharedMethodsElements.check_filter_options(TauxTVA, filter_options_xpath, TVA_row,
				close_xpath, wait_xpath);
		return check;
	}

	// --------------Scenario Generic and principes-----------------
	public void selectAnalysis_resultByprincepes() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par princeps')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_GénériqueAndprinceps_filter_options() {
		more_filter.click();
		String filter_options_xpath = "//div[@id='HELP_lines_product_bcbInfos_genericPrinceps']//div[@class='cg-hover ng-scope layout-row']";
		String Princeps_row = "(//div[@data-code='PRODUCT_REF_GEN_CODE'])[1]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		String wait_xpath = "//span[contains(text(),'Princeps')]";
		List<Boolean> check_princepes = SharedMethodsElements.check_filter_options(Genericandprincepes,
				filter_options_xpath, Princeps_row, close_xpath, wait_xpath);
		return check_princepes;
	}

	// -------------Scenario type de ventes filter----------------------
	public void selectAnalysis_resultByTypeDeVentes() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par type de vente')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_TypeDeVentes_filter_options() {
		more_filter.click();
		String filter_options_xpath = "//div[@id='HELP_isPrescription']//div[@class='cg-hover ng-scope layout-row']";
		String TypeDeVente_row = "(//div[@data-code='PRESCRIPTION_TYPE'])[1]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		String wait_xpath = "//span[contains(text(),'Hors-ordonnance')]";
		List<Boolean> check_Typedevente = SharedMethodsElements.check_filter_options(typeDeVentes, filter_options_xpath,
				TypeDeVente_row, close_xpath, wait_xpath);
		return check_Typedevente;
	}

	// -----------------------------------Scenario-MVG-OS_01-------------------------------
	public void verify_analysis_filter_search(String search_val, String selected_val) {
		analysis_type.click();
		analysis_search_input.sendKeys(search_val);
		List<WebElement> suggested_val = driver.findElements(By.xpath("//div[contains(text(),'Résultats par')]"));
		System.out.println(suggested_val.size());
		Assert.assertNotEquals(suggested_val.size(), 0);
		for (int i = 0; i < suggested_val.size(); i++) {
			Assert.assertTrue(suggested_val.get(i).getText().contains(search_val));
			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
				suggested_val.get(i).click();
				UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
				String analysis_column = driver.findElement(By.xpath("(//div[@role='columnheader'])[1]")).getText();
				analysis_column = analysis_column.toLowerCase();
				Assert.assertTrue(selected_val.toLowerCase().contains(analysis_column));
			}
		}
	}

	// -------------------------------------------Scenario14--------------------------
	public List<Boolean> verify_pharmacies_suggestedValues(String search_val) {
		String Suggested_values_xpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> pharmacies_suggestedvals = SharedMethodsElements.check_filter_search_suggested_values(pharmacies,
				pharmacies_search_input, Suggested_values_xpath, search_val);
		return pharmacies_suggestedvals;
	}

	public List<Boolean> verify_pharmacy_selected_values(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"PHARMA\"]";
		List<Boolean> pharmacies_data_output_check = SharedMethodsElements.check_dataoutput_withSelectedValue(
				Suggested_values_xpath, data_output_xpath, search_val, selected_val);
		return pharmacies_data_output_check;
	}

	// ----------------------------------------------Scenaerio-MVG-fS_04----------------------------------
	public List<Boolean> verify_marche_filter_suggestedValus(String search_val) {
		table_wait();
		String Suggested_values_xpath = "//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> marche_suggestedvals = SharedMethodsElements.check_filter_search_suggested_values(marche,
				marche_search_input, Suggested_values_xpath, search_val);
		return marche_suggestedvals;
	}
//		String data_output_xpath="";
//		SharedMethodsElements.check_dataoutput_withSelectedValue(Suggested_values_xpath,
//				 data_output_xpath, search_val,selected_val);

//		table_wait();
//		marche.click();
//		marche_search_input.sendKeys(search_val);
//		List<WebElement> suggested_val = driver.findElements(
//				By.xpath("//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]"));
//		for (int i = 0; i < suggested_val.size(); i++) {
//			System.out.println(suggested_val.get(i).getText());
////		 	Assert.assertTrue(suggested_val.get(i).getText().toLowerCase().contains(search_val));
//			// case sensitive , suggested values may not contain input
//			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
//				suggested_val.get(i).click();
//				System.out.println("selected");
//				UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
//			} else {
//				System.out.println("not exist");
//			}
//		}
//	}

	// -----------------------------------
	public void verify_favorite_laboratory_filter_options() {
		favorite_labs.click();
	}

	// -------------------------------------------Scenaerio16-----------------------------------
	public void verify_myFavoriteLab_filter_search(String search_val, String selected_val) {
		table_wait();
		favorite_labs.click();
		favorite_labs_search_input.sendKeys(search_val);
		List<WebElement> suggested_val = driver.findElements(
				By.xpath("//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]"));
		for (int i = 0; i < suggested_val.size(); i++) {
			System.out.println(suggested_val.get(i).getText());
			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
				suggested_val.get(i).click();
				System.out.println("selected");
				UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			} else {
				System.out.println("not exist");
			}
		}
	}

	// ----------------------------------------Famille
	// BCB-----------------------------------
	public List<Boolean> verify_FamileBCB_filter_options() throws Exception {
		more_filter.click();
		String filter_options_xpath = "//div[@id='HELP_lines_product_bcbInfos_bcbFamilyCode']//div[@class='ng-scope layout-row']";
		String data_output = "(//div[@data-code=\"product_family_code\"])[1]";
		String wait_xpath = "(//div[@class='cg-font cg-font-refresh fa-spin'])[2]";
		String check_box = "//md-checkbox[@type='checkbox']";
		List<Boolean> FamileBCB_check = SharedMethodsElements.check_filterClassification_options(FamileBCB,
				filter_options_xpath, data_output, check_box, wait_xpath);
		return FamileBCB_check;
	}

	// --------------------------------------------segmentation-----------------------
	public void select_analysisType_withSegmentation() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par segmentation (Niveau 1)')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_Segmentation_filter_options() {
		more_filter.click();
		String filter_options_xpath = "//div[@id='HELP_lines_product_bcbInfos_segmentationCode']//div[@class='ng-scope layout-row']";
		String data_output = "(//div[@data-code='product_segmentation'])[1]";
		String check_box = "//md-checkbox[@type='checkbox']";
		String wait_xpath = "(//div[@class='cg-font cg-font-refresh fa-spin'])[2]";
		List<Boolean> Fseg_check = SharedMethodsElements.check_filterClassification_options(Segmentation,
				filter_options_xpath, data_output, check_box, wait_xpath);
		return Fseg_check;
	}

	// ============================================================================================================
	int x = 0;

	public List<String> filter_container(String newfirst_elment_xpath, String visibleList_xpath, String scroll_xpath,
			int Scroll_pixels, String filter) {
		List<String> elementsList = new ArrayList<String>();
		String firstElementText = "";
		String newFirstElementText = "";
		Boolean listFinished = false;
		while (!listFinished) {
			newFirstElementText = driver.findElement(By.xpath(newfirst_elment_xpath)).getText();
			System.out.println("newFirsteleement " + newFirstElementText);
			if (newFirstElementText.equals(firstElementText)) {
				System.out.println("newFirsteleement " + newFirstElementText);
				if (!firstElementText.equals("")) {
					listFinished = true;
				}
			} else {
				List<WebElement> visibleList = driver.findElements(By.xpath(visibleList_xpath));
				for (int i = 0; i < visibleList.size(); i++) {
					if (!elementsList.contains(visibleList.get(i).getText())) {
						elementsList.add(visibleList.get(i).getText());
						System.out.println(elementsList);
						System.out.println(visibleList.get(i));
						if (filter.contentEquals("analysis")) {
							UtilityClass.set_javaSc_executor(visibleList.get(i));
							UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
							String analysis_column_xpath = "//div[@title=\""
									+ MesVentesData.Expected_mesventes_analysis_column[x] + "\"]";
							WebElement analysis_column = driver.findElement(By.xpath(analysis_column_xpath));
							Assert.assertTrue(analysis_column.isDisplayed());
							Assert.assertTrue(used_filters_name.getText().toLowerCase()
									.contains(visibleList.get(i).getText().toLowerCase()));
							System.out.println(first_chart_data.getText());
							System.out.println(MesVentesData.Expected_mesventes_analysis_chart[x]);
							Assert.assertTrue(first_chart_data.getText()
									.contains(MesVentesData.Expected_mesventes_analysis_chart[x]));
							Assert.assertTrue(second_chart_data.getText()
									.contains(MesVentesData.Expected_mesventes_analysis_chart[x]));
							Assert.assertEquals(MesVentesData.analysis_Type_options[x], visibleList.get(i).getText());
							x++;
							System.out.println("value of x " + x);

						} else if (filter.contentEquals("pharmacies")) {
//							visibleList.get(i).click();
//							UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
//							driver.findElement(By.xpath("//span[@class='cg-font cg-font-times-circle']")).click();

						}
					}
				}
				firstElementText = newFirstElementText;
				System.out.println("first element " + firstElementText);
				System.out.println("visible list size " + visibleList.size());
				System.out.println("scroll");
				UtilityClass.scroll_down(scroll_xpath, Scroll_pixels);
			}
		}
		return elementsList;
	}

	// --------------------------------------------Scenario18-------------------------------------
	public void verify_partmarche_values_selection() {
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
		settings.click();
		List<WebElement> partmarche_values = driver.findElements(
				By.xpath("//button[@class='ui-grid-menu-item ng-binding'][contains(text(),'Part de marché')]"));
		List<String> used_values = new ArrayList<String>();
		for (int i = 0; i < partmarche_values.size(); i++) {
			String value = partmarche_values.get(i).getText().trim();
			System.out.println(value);
			if (!used_values.contains(value)) {
				used_values.add(value);
				Assert.assertEquals(partmarche_values.get(i).getText(),
						MesVentesData.Expected_mesventes_partdemarche[i]);
				partmarche_values.get(i).click();
			}
		}
		for (int i = 0; i < used_values.size(); i++) {
			String click_element = "(//div[@class='ui-grid-cell-contents cg-ellipsis ng-binding'])[" + (i + 11) + "]";
			System.out.println(used_values.get(i));
			String value_xpath = "//div[@title='" + used_values.get(i) + "']";
			try {
				String element_text = UtilityClass.scroll(click_element, value_xpath);
				System.out.println("visible element " + element_text);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// -----------Scenario19-------------------
	public void select_analysisResults_bypharmacies() {
		analysis_type.click();
		UtilityClass.scroll_down("(//div[@class=\"md-virtual-repeat-scroller\"][@role=\"presentation\"])[3]//div", 100);
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par pharmacie')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public void verify_Etat_de_donnees_filter() {
		Etat_de_donnees.click();
		List<WebElement> EtatDeDonnes_options = driver.findElements(
				By.xpath("//div[@id='HELP_DATASTATE_FILTER']//div[@class='cg-hover ng-scope layout-row']"));
		for (int i = EtatDeDonnes_options.size() - 1; i >= 0; i--) {
			System.out.println(EtatDeDonnes_options.get(i).getText());
			EtatDeDonnes_options.get(i).click();
			table_wait();
			String line_number = driver.findElement(By.xpath("//span[@class='text-muted ng-binding ng-scope']"))
					.getText();
			String numberOnly = line_number.replaceAll("[^0-9]", "");
			System.out.println(numberOnly);
			Assert.assertTrue(EtatDeDonnes_options.get(i).getText().contains(numberOnly));
		}
	}

	public void verify_EtatDeDonees_with_periodFilter() {
		period.click();
		List<WebElement> period_filter_options = driver.findElements(
				By.xpath("//div[@id='HELP_PERIOD_FILTER']//div[contains(@class,'cg-ellipsis ng-binding')]"));
		for (int i = 0; i < period_filter_options.size() - 1; i++) {
			System.out.println(period_filter_options.get(i).getText());
			period_filter_options.get(i).click();
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			verify_Etat_de_donnees_filter();
		}
	}

	public void verify_EtatDeDonees_with_pharmacyFilter() {
		Etat_de_donnees.click();
		List<WebElement> EtatDeDonnes_options = driver.findElements(
				By.xpath("//div[@id='HELP_DATASTATE_FILTER']//div[@class='cg-hover ng-scope layout-row']"));
		for (int i = EtatDeDonnes_options.size() - 1; i >= 0; i--) {
			System.out.println(EtatDeDonnes_options.get(i).getText());
			EtatDeDonnes_options.get(i).click();
			table_wait();
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			int x = verify_pharmacies_filter_options();
			String str1 = Integer.toString(x);
			System.out.println(str1);
			Assert.assertTrue(EtatDeDonnes_options.get(i).getText().contains(str1));
		}
	}

	// ---------Laboratoire BCB filter--------------------------------
	public void select_analysisResults_byLaboratoire() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par laboratoire exploitant (Top 1000)')]"))
				.click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public Boolean verify_charactersLimit_search(String search_val) {
		Boolean check_search_limit = SharedMethodsElements.verify_filter_search_character_limits(search_val,
				Laboratoire_BCB, more_filter, Laboratoire_BCB_search_input);
		return check_search_limit;
	}

	public List<Boolean> verify_Laboratoire_BCB_suggestedValues(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_laboFilters']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> suggested_values_check = SharedMethodsElements.check_filter_search_suggested_values(
				Laboratoire_BCB, Laboratoire_BCB_search_input, Suggested_values_xpath, search_val);
		return suggested_values_check;
	}

	public List<Boolean> verify_Adding_laboratoireBCB_filter(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_laboFilters']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code='LABO']";
		List<Boolean> LaboratoireBCB_check = SharedMethodsElements.check_dataoutput_withSelectedValueLaboratoire(
				Suggested_values_xpath, data_output_xpath, search_val, selected_val, AJOUTER_UN_FILTRE_Btn);
		return LaboratoireBCB_check;
	}

	public void verify_output_values_Btn() {
		String[] Btn_options = { "Contient", "Commence par", "Est égal à", "Est différent de", "Ne contient pas" };
		for (int j = 1; j < Btn_options.length; j++) {
			driver.findElement(By.xpath("(//span//div[contains(text(),'" + Btn_options[j - 1] + "')])[2]")).click();
			System.out.println("(//div[contains(text(),'" + Btn_options[j] + "')])[2]");
			driver.findElement(By.xpath("(//div[contains(text(),'" + Btn_options[j] + "')])[2]")).click();
			table_wait();
			List<WebElement> data_output = driver.findElements(
					By.xpath("(//div[@class='ui-grid-viewport ng-isolate-scope'])[1]//div[@class='ng-scope']"));
			for (int x = 0; x < data_output.size(); x++) {
				System.out.println("data output" + data_output.get(x).getText());
			}
		}
	}

	public void verify_laboratory_output_Btn() {

	}

	// ---------------Scenario marque filter-------------------------
	public Boolean verify_marque_search_character_limits(String search_val) {
		Boolean check_search_limit = SharedMethodsElements.verify_filter_search_character_limits(search_val, Marque,
				more_filter, Marque_search_input);
		return check_search_limit;
	}

	public void selectAnalysis_resultByMarque() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par marque (Top 1000)')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_marque_filter_suggestedValues(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_brandName']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> suggested_values_check = SharedMethodsElements.check_filter_search_suggested_values(Marque,
				Marque_search_input, Suggested_values_xpath, search_val);
		return suggested_values_check;
	}

	public List<Boolean> verify_marque_added_filter(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_brandName']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[contains(text(),'"+selected_val+"')]";
		List<Boolean> data_check = SharedMethodsElements.check_dataoutput_withSelectedValue(Suggested_values_xpath,
				data_output_xpath, search_val, selected_val, AJOUTER_UN_FILTRE_Btn);
		return data_check;
	}

	// ------------Scenario nom du produit filter----------------------------
	public Boolean verify_NomDeproduit_search_character_limits(String search_val) {
		Boolean check_search_limit = SharedMethodsElements.verify_filter_search_character_limits(search_val,
				NomDuProduit, more_filter, NomDeproduit_search_input);
		return check_search_limit;
	}

	public void selectAnalysis_resultByproduit() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par produit (Top 1000)')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_NomDuproduit_filter_suggestedValues(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_libelle']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> suggested_values_check = SharedMethodsElements.check_filter_search_suggested_values(NomDuProduit,
				NomDuProduit_search_input, Suggested_values_xpath, search_val);
		return suggested_values_check;
	}

	public List<Boolean> verify_NomDuproduit_added_filter(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_libelle']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code='PRODUCT']";
		List<Boolean> data_check = SharedMethodsElements.check_dataoutput_withSelectedValue(Suggested_values_xpath,
				data_output_xpath, search_val, selected_val, AJOUTER_UN_FILTRE_Btn);
		return data_check;
	}

	// --------------groupe generic filter----------------
	public void selectAnalysis_resultByGroupeGeneric() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par groupe générique')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public void verify_GroupeGeneric_filter_options() {
		more_filter.click();
		GroupeGeneric.click();
		List<WebElement> GroupeGeneric_filter_options = driver.findElements(By.xpath(
				"//div[@id='HELP_lines_product_bcbInfos_genericGroupCodeLabel']//div[contains(@class,'cg-hover ng-scope layout-row')]"));
		for (int i = 0; i < GroupeGeneric_filter_options.size(); i++) {
			System.out.println(GroupeGeneric_filter_options.get(i).getText());
		}
	}

	// ------------------Search in groupe generic-----------------------
	public List<Boolean> verify_groupeGeneric_suggestedValus(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_genericGroupCodeLabel']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> GroupeGeneric_suggValus = SharedMethodsElements.check_filter_search_suggested_values(
				GroupeGeneric, GroupeGeneric_search_box, Suggested_values_xpath, search_val);
		return GroupeGeneric_suggValus;
	}

	public List<Boolean> verify_groupegeneric_filter_add(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_genericGroupCodeLabel']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"product_group_generic_code\"]";
		List<Boolean> GroupeGeneric_dataoutput_check = SharedMethodsElements.check_dataoutput_withSelectedValue(
				Suggested_values_xpath, data_output_xpath, search_val, selected_val, AJOUTER_UN_FILTRE_Btn);
		return GroupeGeneric_dataoutput_check;
	}

	// --------------------------------------------@MVG-05--------------------
	public void select_filter_tobe_saved(String selected_option) {
		period.click();
		String option_xpath = "//div[contains(text(),\"" + selected_option + "\")]";
		driver.findElement(By.xpath(option_xpath)).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public void verify_adding_saved_filters(String filtertype, String filtername) {
		create_savedFilter_Btn.click();
		saved_filter_name.click();
		saved_filter_name.sendKeys(filtername);
		saved_filter_desc.click();
		saved_filter_desc.sendKeys("check if u can use period filter option as saved filter");
		if (filtertype.contentEquals("private")) {
			filterType_selection.click();
			driver.findElement(By.xpath("//div[contains(text(),' Partagé')]")).click();
		} else if (filtertype.contentEquals("shared")) {
			filterType_selection.click();
			driver.findElement(By.xpath("//div[contains(text(),'Privé')]")).click();
		}
		saved_filter_saveBtn.click();
		// check filter if created
		table_wait();
		List<String> saved_filters_name = new ArrayList<>();
		List<WebElement> created_saved_filters = driver
				.findElements(By.xpath("(//div[@class='cg-ellipsis cg-hover ng-binding'])"));
		System.out.println(created_saved_filters.size());
		mysaved_filters.click();
		for (int i = 0; i < created_saved_filters.size(); i++) {
			System.out.println(created_saved_filters.get(i).getText());
			saved_filters_name.add(created_saved_filters.get(i).getText());
		}
		Assert.assertTrue(saved_filters_name.contains(filtername));
	}

	public void verify_using_savedFilter_Asdefault(String filtername, String selected_option) {
		String favorite_xpath = "//div[contains(text(),'" + filtername + "')]/following-sibling::span[2]";
		driver.findElement(By.xpath(favorite_xpath)).click();
		UtilityClass.wait_element_visibility(
				"//span[contains(text(),'Votre filtre a été sélectionné comme filtre par défaut avec succès')]");
		driver.get(driver.getCurrentUrl());
		Assert.assertTrue(used_filters_name.getText().contains(selected_option));
	}

	public void verify_delete_saved_filters(String filtername) {
		mysaved_filters.click();
		String delete_xpath = "//div[contains(text(),'" + filtername + "')]/following-sibling::span[3]";
		driver.findElement(By.xpath(delete_xpath)).click();
		table_wait();
		driver.get(driver.getCurrentUrl());
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
		mysaved_filters.click();
		List<String> saved_filters_name = new ArrayList<>();
		List<WebElement> created_saved_filters = driver
				.findElements(By.xpath("(//div[@class='cg-ellipsis cg-hover ng-binding'])"));
		for (int i = 0; i < created_saved_filters.size(); i++) {
			System.out.println(created_saved_filters.get(i).getText());
			saved_filters_name.add(created_saved_filters.get(i).getText());
		}
		Assert.assertFalse(saved_filters_name.contains(filtername));
	}

//--------------------OTC filter-------------------------------------
	public void selectAnalysisType_withTypedeProduit() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par type de produit')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public Boolean verify_MedicamentOTC_filter_options() {
		String options_xpath = "//div[@id='HELP_lines_product_bcbInfos_isOtc']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"PRODUCT_TYPE_CODE\"][1]";
		String expceted_data = "OTC";
		Boolean check = SharedMethodsElements.verify_OuiAndNon_filter(MedicamentOTC, more_filter, options_xpath,
				data_output_xpath, expceted_data);
		return check;
	}

	// ------------produit cher > ------------------
	public Boolean verify_ProduitCher_filter_options() {
		String options_xpath = "//div[@id='HELP_lines_product_bcbInfos_isExpensive']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"PRODUCT_TYPE_CODE\"][1]";
		String expceted_data = "Produit cher (>450 €HT)";
		Boolean check = SharedMethodsElements.verify_OuiAndNon_filter(ProduitCher, more_filter, options_xpath,
				data_output_xpath, expceted_data);
		return check;
	}

	// ---------------Indication thérapeutiques------------------------
	public void selectAnalysisType_withIndicationthérapeutiques() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Résultats par indication thérapeutique')]")).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public List<Boolean> verify_Indicationthérapeutiques_suggestedValus(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_indicationCIM10Code']//div[@class='cg-hover ng-scope layout-row']";
		List<Boolean> Indication_check = SharedMethodsElements.check_filter_search_suggested_values(
				Indications_thérapeutiques, Indication_search_input, Suggested_values_xpath, search_val);
		return Indication_check;
	}

	public List<Boolean> verify_Indicationthérapeutiques_dataOutput(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_indicationCIM10Code']//div[@class='cg-hover ng-scope layout-row']";
		String data_output_xpath = "//div[@data-code='cim10_code']";
		List<Boolean> IndicationData_check = SharedMethodsElements.check_dataoutput_withSelectedValue(
				Suggested_values_xpath, data_output_xpath, search_val, selected_val, AJOUTER_UN_FILTRE_Btn);
		return IndicationData_check;
	}
}
