package com.Smartrx.Pages;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.Smartrx.Utility.ComparisonData;
import com.Smartrx.Utility.MesPharmaciesData;
import com.Smartrx.Utility.SharedMethodsElements;
import com.Smartrx.Utility.UtilityClass;

public class MesPharmacies extends BaseClass {

	@FindBy(xpath = "//div[contains(text(),'Données de ventes de mes pharmacies')]")
	WebElement MesPharmaciesLink;

	@FindBy(xpath = "//span[contains(text(),'Mes pharmacies')]")
	WebElement tile_title;

	@FindBy(id = "HELP_SUBHEADER_LAYOUT")
	WebElement groupment_name;

	@FindBy(xpath = "//span[contains(text(),' Pharmacies')]//b")
	WebElement number_of_pharmacies;

	@FindBy(xpath = "//md-toolbar[@class='md-toolbar-tools _md _md-toolbar-transitions']")
	WebElement used_filters_name;

	@FindBy(id = "GRID_EXPORT_CSV")
	WebElement csv_downloadIcon;

	@FindBy(id = "GRID_EXPORT_PDF")
	WebElement pdf_downloadIcon;

	@FindBy(xpath = "(//input[@type='search'])[3]")
	WebElement pharmacies_search_input;

	@FindBy(xpath = "(//input[@type='search'])[3]")
	WebElement marche_search_input;

	@FindBy(xpath = "(//input[@type='search'])[3]")
	WebElement LaboratoireBCB_search_input;

	@FindBy(xpath = "//div//span[contains(text(),'Voir plus de filtres')]")
	WebElement more_filter;

	@FindBy(xpath = "//button[contains(text(),'AJOUTER UN FILTRE…')]")
	WebElement Ajouter_Un_Filter;

	// -------saved filters elements-----------------
	@FindBy(xpath = "//button[@class='md-primary md-raised md-button md-ink-ripple']")
	WebElement create_savedFilter_Btn;

	@FindBy(xpath = "//div[@class='cg-background-color-alt-2 cg-hover layout-align-start-center layout-row flex']//span[contains(text(),'Mes filtres sauvegardés ')]")
	WebElement mysaved_filters;

	@FindBy(xpath = "(//input[@name='editItemName'])[1]")
	WebElement saved_filter_name;

	@FindBy(xpath = "(//input[@name='editItemName'])[2]")
	WebElement saved_filter_desc;

	@FindBy(xpath = "//button[contains(text(),'Sauver')]")
	WebElement saved_filter_saveBtn;

	@FindBy(xpath = "//div[@class='ng-scope layout-row']//div[@class='cg-ellipsis cg-hover ng-binding']")
	WebElement created_saved_filter;

	@FindBy(xpath = "//span[@class='cg-font cg-hover cg-font-star-o']")
	WebElement savedFilter_Star;

	@FindBy(xpath = "//span[@class='cg-font cg-font-trash cg-hover']")
	WebElement delete_saved_filter;

	@FindBy(xpath = "(//md-select[@name='editItemName'])")
	WebElement filterType_selection;

	// ----------------filters
	// elements-------------------------------------------------------
	@FindBy(xpath = "//span[contains(text(),\"Type d'analyse\")]")
	WebElement analysis_type;

	@FindBy(xpath = "//span[contains(text(),\"Mes laboratoires préférés \")]")
	WebElement mes_laboratoire_prefere;

	@FindBy(id = "HELP_PERIOD_FILTER")
	WebElement period;

	@FindBy(id = "HELP_EVOL_FILTER")
	WebElement evolution;

	@FindBy(id = "MARKET_SELECT")
	WebElement marche;

	@FindBy(xpath = "//div[@id='HELP_tenant_group']")
	WebElement groupments;

	@FindBy(id = "HELP_tenant")
	WebElement pharmacies;
	@FindBy(xpath = "//div[@id='HELP_DATASTATE_FILTER']")
	WebElement Etat_de_donnees;

	@FindBy(xpath = "//span[contains(text(),'Laboratoire BCB')]")
	WebElement LaboratoireBCB;

	@FindBy(id = "HELP_lines_vatRateFixed")
	WebElement TauxTVA;

	@FindBy(id = "HELP_lines_product_bcbInfos_bcbFamilyCode")
	WebElement FamilleBCB;

	@FindBy(id = "HELP_lines_product_bcbInfos_segmentationCode")
	WebElement Segmentation;

	@FindBy(id = "HELP_lines_product_bcbInfos_atcClassCode")
	WebElement ClasseATC;

	@FindBy(id = "HELP_lines_product_bcbInfos_brandName")
	WebElement Marque;

	@FindBy(id = "HELP_lines_product_bcbInfos_libelle")
	WebElement NomDeProduit;
	
	@FindBy (id="HELP_lines_product_bcbInfos_genericPrinceps")
	WebElement GenericAndPrincipes;
	
	@FindBy (id="HELP_isPrescription")
	WebElement TypeDeVentes;
	
	@FindBy (id="HELP_lines_product_bcbInfos_isOtc")
	WebElement MedicamentOTC;

	// ------------filter search box-----------------
	@FindBy(xpath = "(//input[@type='search'])[3]")
	WebElement Marque_searchBox;

	@FindBy(xpath = "(//input[@type='search'])[3]")
	WebElement NomDeProduit_searchBox;

	public MesPharmacies() {
		PageFactory.initElements(driver, this);
	}

	public void table_wait() {
		By loadingProgress = By.xpath("div[@ng-show=\"chart.loading\"]");
		UtilityClass.waitExpectedConditionsElementContainsAttributeText(loadingProgress, "aria-hidden", "true");
	}

	// ------Background------------------
	public void Open_Mes_pharmacies_tile() {
		UtilityClass.set_javaSc_executor(MesPharmaciesLink);
	}

	// ------------- Tile data ---------------------------------------
	public String verify_tile_title() {
		return tile_title.getText();
	}

	public String verify_numer_of_pharmacies() {
		return number_of_pharmacies.getText();
	}

	public String verify_groupment_name() {
		return groupment_name.getText();
	}

	public List<String> verify_table_columns() {
		List<String> column_header = new ArrayList<>();
		for (int i = 0; i < MesPharmaciesData.column_header_default_xpath.length; i++) {
			String column_xpath = "//div[@title=\"" + MesPharmaciesData.column_header_default_xpath[i] + "\"]";
			System.out.println(column_xpath);
			String click_element = "(//div[@class='ui-grid-cell-contents ng-binding cg-evolution-up'])[1]";
			System.out.println(click_element);
			try {
				String element = UtilityClass.scroll(click_element, column_xpath);
				System.out.println(element);
				column_header.add(element);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return column_header;
	}

	// -------------default filters in tile--------------------
	public List<String> verify_default_filters_selection() {
		analysis_type.click();
		mes_laboratoire_prefere.click();
		period.click();
		evolution.click();
		Etat_de_donnees.click();
		List<String> default_filters_selected = new ArrayList<>();
		List<WebElement> default_filters = driver
				.findElements(By.xpath("//div[contains(@class,'cg-sidenav-text-selected')]"));
		System.out.println(default_filters.size());
		for (int i = 0; i < MesPharmaciesData.Expected_mespharmacies_default_filters.length; i++) {
			System.out.println(default_filters.get(i).getText());
			default_filters_selected.add(default_filters.get(i).getText());
			String default_filter_color = default_filters.get(i).getCssValue("color");
			Assert.assertEquals(ComparisonData.Expected_default_filter_color, default_filter_color);
		}
		return default_filters_selected;
	}

	// ----------------period filter------------------------
	public List<Boolean> check_period_filter_options() {
		String period_filter_options = "//div[@id='HELP_PERIOD_FILTER']//div[contains(@class,'cg-ellipsis ng-binding')]";
		List<Boolean> period_check = SharedMethodsElements.check_filter_options(period, period_filter_options,
				used_filters_name, "2");
		return period_check;
	}

	// --------period search functionality-------------------
	public void verify_specific_period_selection(String date_debut, String date_fin) {
		String date_box1 = "(//input[@class='md-datepicker-input'])[1]";
		String date_box2 = "(//input[@class='md-datepicker-input'])[2]";
		SharedMethodsElements.date_personalisee(period, date_box1, date_box2, date_debut, date_fin);
	}

	// ----------------Evolution filter------------------
	public List<Boolean> verify_evolution_filter_options() {
		String evolution_filter_options = "//div[@id='HELP_EVOL_FILTER']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> evolution_check = SharedMethodsElements.check_filter_options(evolution, evolution_filter_options,
				used_filters_name, "2");
		return evolution_check;
	}

	public void verify_specific_evolution_selection(String date_debut, String date_fin) {
		String date_box1 = "(//input[@class='md-datepicker-input'])[3]";
		String date_box2 = "(//input[@class='md-datepicker-input'])[4]";
		SharedMethodsElements.date_personalisee(evolution, date_box1, date_box2, date_debut, date_fin);
	}

	// ------------------------------------------Scenario11---------------------------------------------------------------------------------------------------------
	public void select_filter_tobe_saved(String selected_option) {
		evolution.click();
		String option_xpath = "//div[contains(text(),\"" + selected_option + "\")]";
		driver.findElement(By.xpath(option_xpath)).click();
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
	}

	public void verify_adding_saved_filters(String filtertype, String filtername) {
		create_savedFilter_Btn.click();
		saved_filter_name.click();
		saved_filter_name.sendKeys(filtername);
		saved_filter_desc.click();
		saved_filter_desc.sendKeys("check if u can use period filter option as saved filter");
		if (filtertype.contentEquals("private")) {
			filterType_selection.click();
			driver.findElement(By.xpath("//div[contains(text(),' Partagé')]")).click();
		} else if (filtertype.contentEquals("shared")) {
			filterType_selection.click();
			driver.findElement(By.xpath("//div[contains(text(),'Privé')]")).click();
		}
		saved_filter_saveBtn.click();
		// check filter if created
		table_wait();
		List<String> saved_filters_name = new ArrayList<>();
		List<WebElement> created_saved_filters = driver
				.findElements(By.xpath("(//div[@class='cg-ellipsis cg-hover ng-binding'])"));
		System.out.println(created_saved_filters.size());
		mysaved_filters.click();
		for (int i = 0; i < created_saved_filters.size(); i++) {
			System.out.println(created_saved_filters.get(i).getText());
			saved_filters_name.add(created_saved_filters.get(i).getText());
		}
		Assert.assertTrue(saved_filters_name.contains(filtername));
	}

	public void verify_using_savedFilter_Asdefault(String filtername, String selected_option) {
		String favorite_xpath = "//div[contains(text(),'" + filtername + "')]/following-sibling::span[2]";
		driver.findElement(By.xpath(favorite_xpath)).click();
		UtilityClass.wait_element_visibility(
				"//span[contains(text(),'Votre filtre a été sélectionné comme filtre par défaut avec succès')]");
		driver.get(driver.getCurrentUrl());
		Assert.assertTrue(used_filters_name.getText().contains(selected_option));
	}

	public void verify_delete_saved_filters(String filtername) {
		mysaved_filters.click();
		String delete_xpath = "//div[contains(text(),'" + filtername + "')]/following-sibling::span[3]";
		driver.findElement(By.xpath(delete_xpath)).click();
		table_wait();
		driver.get(driver.getCurrentUrl());
		UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
		mysaved_filters.click();
		List<String> saved_filters_name = new ArrayList<>();
		List<WebElement> created_saved_filters = driver
				.findElements(By.xpath("(//div[@class='cg-ellipsis cg-hover ng-binding'])"));
		for (int i = 0; i < created_saved_filters.size(); i++) {
			System.out.println(created_saved_filters.get(i).getText());
			saved_filters_name.add(created_saved_filters.get(i).getText());
		}
		Assert.assertFalse(saved_filters_name.contains(filtername));
	}

	// -----------------------------------------Scenario19------------------------------------------------------------------------------------------------------
	public void verify_Etat_de_donnees_filter() {
		table_wait();
		Etat_de_donnees.click();
		List<WebElement> EtatDeDonnes_options = driver.findElements(
				By.xpath("//div[@id='HELP_DATASTATE_FILTER']//div[@class='cg-hover ng-scope layout-row']"));
		for (int i = EtatDeDonnes_options.size() - 1; i >= 0; i--) {
			System.out.println(EtatDeDonnes_options.get(i).getText());
			EtatDeDonnes_options.get(i).click();
			table_wait();
			String line_number = driver.findElement(By.xpath("//span[@class='text-muted ng-binding ng-scope']"))
					.getText();
			String numberOnly = line_number.replaceAll("[^0-9]", "");
			int number = Integer.parseInt(numberOnly);
			Assert.assertTrue("number of raws incorrect", EtatDeDonnes_options.get(i).getText().contains(numberOnly));
			List<WebElement> period_ventes_complete = driver.findElements(
					By.xpath("//div[@class='ui-grid-cell-contents']//span[@class='cg-font cg-font-check']"));
			List<WebElement> period_ventes_incomplete = driver.findElements(
					By.xpath("//div[@class='ui-grid-cell-contents']//span[@class='cg-font cg-font-calendar']"));
			if (EtatDeDonnes_options.get(i).getText().contains("Données complètes")) {
				try {
					Assert.assertEquals(period_ventes_complete.size(), (number + 1));
					Assert.assertEquals(period_ventes_incomplete.size(), 0);
				} catch (Exception e) {
					System.out.println("Assertion complete failed ");
				}

			} else if (EtatDeDonnes_options.get(i).getText().contains("Données incomplètes")) {
				try {
					Assert.assertEquals(period_ventes_incomplete.size(), number + 1);
					Assert.assertEquals(period_ventes_complete.size(), 0);
				} catch (Exception e) {
					System.out.println("Assertion incomplete failed ");
				}

			}
		}
	}

	public void verify_EtatDeDonees_with_periodFilter() {
		period.click();
		List<WebElement> period_filter_options = driver.findElements(
				By.xpath("//div[@id='HELP_PERIOD_FILTER']//div[contains(@class,'cg-ellipsis ng-binding')]"));
		for (int i = 0; i < period_filter_options.size() - 1; i++) {
			System.out.println(period_filter_options.get(i).getText());
			period_filter_options.get(i).click();
			UtilityClass.wait_element_visibility("(//div[@role='columnheader'])[1]");
			verify_Etat_de_donnees_filter();
		}
	}
	//////////////////////////////////////////////////////////

	public void verify_MesPharmacies_downloadCSV() {
		csv_downloadIcon.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UtilityClass.check_downloadedFile_size(0);
	}

	public void verify_MesPharmacies_downloadPDF() {
		pdf_downloadIcon.click();
	}

	// ----------Pharmacy filter----------------------------
	public List<Boolean> verify_pharmacies_filter_options() {
		String filter_options_xpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"PHARMA\"]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		String wait_xpath = data_output_xpath;
		List<Boolean> pharmacies_options_check = SharedMethodsElements.check_filter_options(pharmacies,
				filter_options_xpath, data_output_xpath, close_xpath, wait_xpath);
		return pharmacies_options_check;
	}

	public List<Boolean> checkPharmacy_filterduplicated_options() {
		String filter_options_xpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> pharmacies_filter_duplicationCheck = SharedMethodsElements
				.Check_filterOptions_duplication(pharmacies, filter_options_xpath);
		return pharmacies_filter_duplicationCheck;
	}
	// ----------pharmacy filter search ---------------------

	public List<Boolean> verify_pharmacies_filter_search_suggestedValues(String search_val) {
		String Suggested_values_xpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> check_suggestedValues = SharedMethodsElements.check_filter_search_suggested_values(pharmacies,
				pharmacies_search_input, Suggested_values_xpath, search_val);
		return check_suggestedValues;
	}

	public List<Boolean> Verify_pharmacies_filter_search_selectedValues(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_tenant']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"PHARMA\"]";
		List<Boolean> CheckDataOutput = SharedMethodsElements.check_dataoutput_withSelectedValue(Suggested_values_xpath,
				data_output_xpath, search_val, selected_val);
		return CheckDataOutput;
	}

	// -------------marches filter search---------------------
	public List<Boolean> verify_marches_filter_search_suggestedValues(String search_val) {
		String Suggested_values_xpath = "//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> check_suggestedValues = SharedMethodsElements.check_filter_search_suggested_values(marche,
				marche_search_input, Suggested_values_xpath, search_val);
		return check_suggestedValues;
	}

	public List<Boolean> Verify_marches_filter_search_selectedValues(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='MARKET_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "//div[@data-code=\"PHARMA\"]";
		List<Boolean> CheckDataOutput = SharedMethodsElements.check_dataoutput_withSelectedValue(Suggested_values_xpath,
				data_output_xpath, search_val, selected_val);
		return CheckDataOutput;
	}

	// ----------Last date transmission--------------
	public List<String> lastDate_demo() {
		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
		String click_element = "(//div[@class='ui-grid-cell-contents ng-binding cg-evolution-up'])[3]";
		String xpath_value = "//span[contains(text(),'Date dernière vente transmise ')]";
		try {
			UtilityClass.scroll(click_element, xpath_value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<WebElement> lastDate_cells = driver.findElements(By.xpath("//div[@data-type='DT']"));
		List<String> Last_Date = new ArrayList<>();
		for (int i = 0; i < lastDate_cells.size(); i++) {
			if (lastDate_cells.get(i).getText().length() > 0) {
				Last_Date.add(lastDate_cells.get(i).getText());
			}
		}
		return Last_Date;
	}

	public List<Boolean> verify_lastDateTransmission(String first_date) throws ParseException {
		List<String> lastDate_cells = lastDate_demo();
		List<WebElement> lastDate_cells_color = driver.findElements(By.xpath("//div[@data-type='DT']/.."));
		System.out.println("number of cells--" + lastDate_cells.size());
		List<Boolean> LastDate_Colorcheck = new ArrayList<>();
		for (int i = 0; i < lastDate_cells.size(); i++) {
			String last_transmissiondate = lastDate_cells.get(i);
			String last_transmissiondate_color = lastDate_cells_color.get(i).getCssValue("background-color");
			last_transmissiondate = last_transmissiondate.substring(0, last_transmissiondate.length() - 6);
			System.out.println("date --" + last_transmissiondate);
			long date_difference = UtilityClass.find_difference_betweenTwoDates(first_date, last_transmissiondate);
			System.out.println(date_difference);
			if (date_difference == 0) {
				Boolean sameday_check = last_transmissiondate_color
						.contentEquals(MesPharmaciesData.Lastdate_transmission_colors[0]);
				LastDate_Colorcheck.add(sameday_check);
			} else if (date_difference > 0 && date_difference <= 7) {
				Boolean withinWeek_check = last_transmissiondate_color
						.contentEquals(MesPharmaciesData.Lastdate_transmission_colors[1]);
				LastDate_Colorcheck.add(withinWeek_check);
			} else if (date_difference > 7) {
				Boolean moreweek_check = last_transmissiondate_color
						.contentEquals(MesPharmaciesData.Lastdate_transmission_colors[2]);
				LastDate_Colorcheck.add(moreweek_check);
			}
		}

		return LastDate_Colorcheck;
	}

	public List<Boolean> verify_TVA_filter_options() {
		String TVA_options = "//div[@id='HELP_lines_vatRateFixed']//div[@class='cg-hover ng-scope layout-row']";
		String wait_xpath = "//span[contains(text(),'TVA 2,1 %')]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		List<Boolean> TVAfilter_check = check_if_filter_options_working(TauxTVA, TVA_options, wait_xpath, close_xpath);
		return TVAfilter_check;
	}

	public List<Boolean> verify_familleBCB_filter_options() {
		String familleBCB_options = "//md-checkbox[@type='checkbox']";
		String wait_xpath = "//span[contains(text(),'Dermocosmétique')]";
		String close_xpath = "//md-checkbox[@type='checkbox']";
		List<Boolean> familleBCBfilter_check = check_if_filterClassification_options_working(FamilleBCB,
				familleBCB_options, wait_xpath, close_xpath);
		return familleBCBfilter_check;
	}

	public List<Boolean> verify_Segmentation_filter_options() {
		String Segmentation_options = "//md-checkbox[@type='checkbox']";
		String wait_xpath = "//span[contains(text(),'Système respiratoire')]";
		String close_xpath = "//md-checkbox[@type='checkbox']";
		List<Boolean> Segmentationfilter_check = check_if_filterClassification_options_working(Segmentation,
				Segmentation_options, wait_xpath, close_xpath);
		return Segmentationfilter_check;
	}

	public List<Boolean> verify_ClasseATC_filter_options() {
		String ClasseATC_options = "//md-checkbox[@type='checkbox']";
		String wait_xpath = "//span[contains(text(),'VOIES DIGESTIVES')]";
		String close_xpath = "//md-checkbox[@type='checkbox']";
		List<Boolean> ClasseATCfilter_check = check_if_filterClassification_options_working(ClasseATC,
				ClasseATC_options, wait_xpath, close_xpath);
		return ClasseATCfilter_check;
	}

	// --------search in Laboratoire BCB--------------------------
	public Boolean verify_LaboratoireBCB_search_character_limits(String search_val) {
		Boolean check_error_message = SharedMethodsElements.verify_filter_search_character_limits(search_val,
				LaboratoireBCB, more_filter, LaboratoireBCB_search_input);
		return check_error_message;
	}

	public List<Boolean> verify_LaboratoireBCB_filter_search_suggestedValues(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_laboFilters']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> check_suggestedValues = SharedMethodsElements.check_filter_search_suggested_values(LaboratoireBCB,
				LaboratoireBCB_search_input, Suggested_values_xpath, search_val);
		return check_suggestedValues;
	}

	public List<Boolean> verify_LaboratoireBCB_filter_addedValues(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_laboFilters']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "(//div[@data-code='PHARMA'])";
		List<Boolean> check_data = check_dataoutput_withSelectedValue(Suggested_values_xpath, data_output_xpath,
				search_val, selected_val);
		return check_data;
	}

	// ------------Marque filter----------------
	public Boolean verify_Marque_search_character_limits(String search_val) {
		Boolean check_error_message = SharedMethodsElements.verify_filter_search_character_limits(search_val, Marque,
				more_filter, Marque_searchBox);
		return check_error_message;
	}

	public List<Boolean> verify_Marque_filter_search_suggestedValues(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_brandName']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> check_suggestedValues = SharedMethodsElements.check_filter_search_suggested_values(Marque,
				Marque_searchBox, Suggested_values_xpath, search_val);
		return check_suggestedValues;
	}

	public List<Boolean> verify_Marque_filter_addedValues(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_brandName']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "(//div[@data-code='PHARMA'])";
		List<Boolean> check_data = check_dataoutput_withSelectedValue(Suggested_values_xpath, data_output_xpath,
				search_val, selected_val);
		return check_data;
	}
	
	//----------------------Nom du produit------------------------------------------
	public Boolean verify_NomDuProduit_search_character_limits(String search_val) {
		Boolean check_error_message = SharedMethodsElements.verify_filter_search_character_limits(search_val, NomDeProduit,
				more_filter,  NomDeProduit_searchBox);
		return check_error_message;
	}

	public List<Boolean> verify_NomDuProduit_filter_search_suggestedValues(String search_val) {
		more_filter.click();
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_libelle']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		List<Boolean> check_suggestedValues = SharedMethodsElements.check_filter_search_suggested_values( NomDeProduit,
				 NomDeProduit_searchBox, Suggested_values_xpath, search_val);
		return check_suggestedValues;
	}

	public List<Boolean> verify_NomDuProduit_filter_addedValues(String search_val, String selected_val) {
		String Suggested_values_xpath = "//div[@id='HELP_lines_product_bcbInfos_libelle']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String data_output_xpath = "(//div[@data-code='PHARMA'])";
		List<Boolean> check_data = check_dataoutput_withSelectedValue(Suggested_values_xpath, data_output_xpath,
				search_val, selected_val);
		return check_data;
	}
	//------------------generic and principes-----------------
	public List<Boolean> verify_GenericandPrincipes_filter_options() {
		String GenericandPrincipes_options = "//div[@id='HELP_lines_product_bcbInfos_genericPrinceps']//div[@class='cg-hover ng-scope layout-row']";
		String wait_xpath = "//span[contains(text(),'Princeps')]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		List<Boolean> GenericandPrincipesfilter_check = check_if_filter_options_working(GenericAndPrincipes, GenericandPrincipes_options, wait_xpath, close_xpath);
		return GenericandPrincipesfilter_check;
	}
	
	//-----------------type de ventes-------------------------
	public List<Boolean> verify_TypeDeVentes_filter_options() {
		String TypeDeVentes_options = "//div[@id='HELP_isPrescription']//div[@class='cg-hover ng-scope layout-row']";
		String wait_xpath = "//span[contains(text(),'Hors-ordonnance')]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		List<Boolean> GenericandPrincipesfilter_check = check_if_filter_options_working(TypeDeVentes, TypeDeVentes_options, wait_xpath, close_xpath);
		return GenericandPrincipesfilter_check;
	}
	
	//----------Medicament OTC-----------------------
	public List<Boolean> verify_MedicamentOTC_filter_options(){
		String MedicamentOTC_options = "//div[@id='HELP_lines_product_bcbInfos_isOtc']//div[@class='cg-hover ng-scope layout-row']";
		String wait_xpath = "//span[contains(text(),'Non')]";
		String close_xpath = "//span[@class='cg-font cg-font-times-circle']";
		List<Boolean> GenericandPrincipesfilter_check = check_if_filter_options_working(MedicamentOTC, MedicamentOTC_options, wait_xpath, close_xpath);
		return GenericandPrincipesfilter_check;
	}
	

	public List<Boolean> check_if_filter_options_working(WebElement filter, String options_xpath, String wait_xpath,
			String close_xpath) {
		more_filter.click();
		filter.click();
		UtilityClass.wait_element_visibility(wait_xpath);
		List<WebElement> filter_options = driver.findElements(By.xpath(options_xpath));
		List<Boolean> filteroption_check = new ArrayList<>();
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println(filter_options.get(i).getText());
			filter_options.get(i).click();
			UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
			List<WebElement> data_pharmacies = driver.findElements(By.xpath("//div[@data-code=\"PHARMA\"]"));
			Boolean filter_check = data_pharmacies.size() > 0;
			filteroption_check.add(filter_check);
			driver.findElement(By.xpath(close_xpath)).click();
			UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
		}
		return filteroption_check;
	}

	public List<Boolean> check_if_filterClassification_options_working(WebElement filter, String options_xpath,
			String wait_xpath, String close_xpath) {
		more_filter.click();
		filter.click();
		UtilityClass.wait_element_visibility(wait_xpath);
		List<WebElement> filter_options = driver.findElements(By.xpath(options_xpath));
		List<Boolean> filteroption_check = new ArrayList<>();
		for (int i = 0; i < filter_options.size(); i++) {
			System.out.println(filter_options.get(i).getText());
			filter_options.get(i).click();
			UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
			List<WebElement> data_pharmacies = driver.findElements(By.xpath("//div[@data-code=\"PHARMA\"]"));
			Boolean filter_check = data_pharmacies.size() > 0;
			filteroption_check.add(filter_check);
			filter_options.get(i).click();
			UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
		}
		return filteroption_check;
	}

	public List<Boolean> check_dataoutput_withSelectedValue(String Suggested_values_xpath, String data_output_xpath,
			String search_val, String selected_val) {
		List<WebElement> suggested_val = driver.findElements(By.xpath(Suggested_values_xpath));
		List<Boolean> check_dataOutput = new ArrayList<>();
		for (int i = 0; i < suggested_val.size(); i++) {
			System.out.println("selected value " + selected_val);
			if (suggested_val.get(i).getText().contentEquals(selected_val)) {
				suggested_val.get(i).click();
				Ajouter_Un_Filter.click();
				UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])");
				List<WebElement> data = driver.findElements(By.xpath(data_output_xpath));
				for (int x = 0; x < data.size(); x++) {
					System.out.println("data_output" + data.get(x).getText());
					Boolean check_data = data.size() > 0;
					check_dataOutput.add(check_data);
				}
			}
		}
		return check_dataOutput;
	}
	
	public void check_catch(){
		String newfirst_elment_xpath="(//div[@data-code='PHARMA'])[1]";
		String visibleList_xpath="(//div[@data-code='PHARMA'])";
		String Scroll="(//div[@role='rowgroup'])[4]";
		filter_container(newfirst_elment_xpath,visibleList_xpath,Scroll,100);
	}

	// ============================================================================================================
		int x = 0;
		public List<String> filter_container(String newfirst_elment_xpath, String visibleList_xpath, String scroll_xpath,
				int Scroll_pixels) {
			List<String> elementsList = new ArrayList<String>();
			String firstElementText = "";
			String newFirstElementText = "";
			Boolean listFinished = false;
			while (!listFinished) {
				newFirstElementText = driver.findElement(By.xpath(newfirst_elment_xpath)).getText();
				System.out.println("newFirsteleement " + newFirstElementText);
				if (newFirstElementText.equals(firstElementText)) {
					System.out.println("newFirsteleement " + newFirstElementText);
					if (!firstElementText.equals("")) {
						listFinished = true;
					}
				} else {
					List<WebElement> visibleList = driver.findElements(By.xpath(visibleList_xpath));
					for (int i = 0; i < visibleList.size(); i++) {
						if (!elementsList.contains(visibleList.get(i).getText())) {
							elementsList.add(visibleList.get(i).getText());
							System.out.println(elementsList);
							System.out.println(visibleList.get(i));
						}
					}
					firstElementText = newFirstElementText;
					System.out.println("first element " + firstElementText);
					System.out.println("visible list size " + visibleList.size());
					System.out.println("scroll");
					UtilityClass.scroll_down(scroll_xpath, Scroll_pixels);
				}
			}
			return elementsList;
		}

}
