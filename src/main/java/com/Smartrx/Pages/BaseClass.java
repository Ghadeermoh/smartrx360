package com.Smartrx.Pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.Smartrx.Utility.UtilityClass;

public class BaseClass {
	public static WebDriver driver;
	public static Properties prop;
//	public static String download_files_path="C:\\Users\\gmohamed\\Smartrx360Group\\fileDownloaded";
	public static File folder;

	public BaseClass() {

		prop = new Properties();
		try {
			FileInputStream ip = new FileInputStream(UtilityClass.config_path);

			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void init() {
		// -------------------------
		folder = new File(UUID.randomUUID().toString());
		folder.mkdir();
		// ---------------------------

		String browsername = prop.getProperty("browser");
		if (browsername.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", UtilityClass.chrome_path);

			// ---------------------------------
			ChromeOptions options = new ChromeOptions();
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.default_directory", folder.getAbsolutePath());
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setCapability(ChromeOptions.CAPABILITY, options);
			// -------------------------------
			driver = new ChromeDriver(cap);
		} else if (browsername.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", UtilityClass.firefox_path); // give location of the driver
			driver = new FirefoxDriver();
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(UtilityClass.Page_Load_TimeOut, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(UtilityClass.Implicit_Wait, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
	}

}
