package com.Smartrx.Pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Smartrx.Utility.ComparisonData;
import com.Smartrx.Utility.ObservatoireData;
import com.Smartrx.Utility.UtilityClass;


public class ObservatoireDesPrix extends BaseClass{
	
	@FindBy  (id="PHARMA_COMPARE_PRICE")
	WebElement  ObservatoireDesPrixLink;
	
	@FindBy (xpath="//span[contains(text(),'Observatoire des prix')]")
	WebElement ObservatoireTileName;
	//---------filters elements
	@FindBy (id="PHARMA_NUMBER_SELECT")
	WebElement comparison;
	
	@FindBy (id ="TYPE_SELECT")
	WebElement comparison_segment;
	
	@FindBy (id="MARKET_SELECT")
	WebElement marche;
	
	@FindBy (id="PRICE_TYPE_SELECT")
	WebElement prix_type;
	
	@FindBy (id ="PERIOD_SELECT")
	WebElement period;
	
	@FindBy (id="PHARMA_SELECT")
	WebElement pharmacy;
	
	public ObservatoireDesPrix() {
		PageFactory.initElements(driver, this);
	}
	
	//-------------background--------------
	public void open_observatoire_tile() {
		ObservatoireDesPrixLink.click();
	}
	
	//--------Scenario1------------
	public void verify_tile_name() {
		Assert.assertEquals(ObservatoireTileName.getText(), ObservatoireData.Expected_tile_name);
	}
	
	//--------Scenario2-------------
    public void verify_default_filters() {
    	comparison.click();
    	comparison_segment.click();
    	marche.click();
    	prix_type.click();
    	period.click();
    	pharmacy.click();
    	
    	List <WebElement> default_filters=driver.findElements(By.xpath("//div[contains(@class,'cg-sidenav-text-selected')]"));
		
		System.out.println(default_filters.size());
		for (int i=0;i<default_filters.size();i++)
		{
			    System.out.println(default_filters.get(i).getText());
//			    Assert.assertEquals(default_filters.get(i).getText(),ObservatoireData.Expected_observatoire_default_filters[i]);
			    Assert.assertEquals(default_filters.get(i).getCssValue("color"), ComparisonData.Expected_default_filter_color);
		}
    }
    
    //-------Scenario3---------------
    public void verify_marche_filter_options() {
    	pharmacy.click();
List <WebElement>marche_options=driver.findElements(By.xpath("//div[contains(@class,'cg-hover ng-scope layout-row')]"));
		
		for(int i=0;i<marche_options.size();i++)
		{
			marche_options.get(i).click();
			marche_options.get(i).getText();
         }
    }

}
