package com.Smartrx.Pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Smartrx.Utility.ComparisonData;
import com.Smartrx.Utility.SharedMethodsElements;
import com.Smartrx.Utility.UtilityClass;

public class Comparaison extends BaseClass {
	@FindBy(id = "PHARMA_COMPARE")
	WebElement ComparisonLink;

	@FindBy(xpath = "//span[contains(text(),'Comparaison')]")
	WebElement comparisonTileName;

	@FindBy(xpath = "//div[@class='md-subheader-content']//span[1]")
	WebElement Groupment_name;

	@FindBy(xpath = "//span [contains(text(),'AIDE')]")
	WebElement HelpLink;

	// -----filters elements----------
	@FindBy(id = "TYPE_SELECT")
	WebElement comparison_segment;

	@FindBy(id = "SUBGROUP_SELECT")
	WebElement Groupment;

	@FindBy(xpath = "//span[contains(text(),'Période')]")
	WebElement period;

	@FindBy(id = "PERIOD_REFERENCE_SELECT")
	WebElement reference_period;

	@FindBy(id = "PHARMA_SELECT")
	WebElement pharmacy;

	@FindBy(id = "STR_MARKET_DISPLAY_OPTIONS")
	WebElement analysis_type;

	@FindBy(xpath = "//span[@class='cg-hover cg-font cg-font-chevron-left']")
	WebElement filters_menu;

	// --------table titles elements-------
	@FindBy(xpath = "//span[contains(text(),'Ventes')]")
	WebElement FirstTable_title;

	@FindBy(xpath = "//span[contains(text(),'TVA')]")
	WebElement SecondTable_title;

	@FindBy(xpath = "//span[contains(text(),'Substitution en nombre de boîtes')]")
	WebElement ThirdTable_title;

	@FindBy(xpath = "//span[contains(text(),'Famille')]")
	WebElement FourthTable_title;

	// ------used filters name elements ---------

	@FindBy(xpath = "//div[contains(text(),'vs')]")
	WebElement used_filters_name;

	@FindBy(xpath = "//div[contains(text(),'vs')]//span[1]")
	WebElement pharmacy_used_filter_name;

	@FindBy(xpath = "//div[contains(text(),'vs')]//span[2]")
	WebElement segment_used_filter_name;

	@FindBy(xpath = "//div[contains(text(),'vs')]//span[5]")
	WebElement period_used_filter_name;
	
	@FindBy (xpath="(//input[@type='search'])[6]")
	WebElement Pharmacy_search_box;

	public Comparaison() {
		PageFactory.initElements(driver, this);
	}

	public void table_load_wait() {
		By loadingProgress = By.xpath("div[@ng-show=\"chart.loading\"]");
		UtilityClass.waitExpectedConditionsElementContainsAttributeText(loadingProgress, "aria-hidden", "true");

	}

//	public void wait_table() {
//		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])[1]");
//		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])[2]");
//		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])[3]");
//		UtilityClass.wait_element_invisibility("(//div[@class='cg-font cg-font-refresh fa-spin'])[4]");
//	}
	// --------background-------
	public void open_comparaison_tile() {
		ComparisonLink.click();
	}

	// ---------default data -----------
	public String verify_tile_title() {
		return comparisonTileName.getText();
	}

	public String verify_groupment_name() {
		return Groupment_name.getText();
	}

	public void verify_default_filters_selection() {
		analysis_type.click();
		reference_period.click();
		comparison_segment.click();
		period.click();
		pharmacy.click();
		List<WebElement> default_filters = driver
				.findElements(By.xpath("//div[contains(@class,'cg-sidenav-text-selected')]"));
		for (int i = 0; i < ComparisonData.Expected_comparison_default_filters.length - 1; i++) {
			System.out.println(default_filters.get(i).getText());
			Assert.assertEquals(default_filters.get(i).getText(),
					ComparisonData.Expected_comparison_default_filters[i]);
			Assert.assertEquals(default_filters.get(i).getCssValue("color"),
					ComparisonData.Expected_default_filter_color);
		}
	}

	// -----------verify for each table --------------------------
	public List<String> verify_tables_titles() {
		List<WebElement> tables_titles = driver
				.findElements(By.xpath("//div[@class='ng-scope flex']//div[@class='ng-scope layout-row flex']//span"));
		List<String> table_titles = new ArrayList<>();
		System.out.println(tables_titles.size());
		for (int i = 0; i < tables_titles.size(); i++) {
			System.out.println(tables_titles.get(i).getText());
			table_titles.add(tables_titles.get(i).getText());
		}
		return table_titles;
	}

	// ---------verify table buttons-----------------------
	public void verify_default_buttons_selections() {
		List<WebElement> defualt_buttons_selected = driver.findElements(
				By.xpath("//button[@class='md-raised cg-small-button md-button md-ink-ripple md-primary']"));
		for (int i = 0; i < defualt_buttons_selected.size(); i++) {
			Assert.assertEquals(defualt_buttons_selected.get(i).getCssValue("background-color"),
					ComparisonData.Expected_default_buttons_color);
		}
	}

	// ---------verify table columns-----------------------
	public List<String> gather_column() {
		List<WebElement> column_header1 = driver
				.findElements(By.xpath("//div[@class='ui-grid-cell-contents ui-grid-header-cell-primary-focus']"));
		List<String> Analysis1_columns = new ArrayList<>();
		for (int i = 0; i < column_header1.size(); i++) {
			System.out.println(column_header1.get(i).getText());
			Analysis1_columns.add(column_header1.get(i).getText());
		}
		return Analysis1_columns;
	}

	// ---------verify table rows-----------------------
	public void verify_rows_analysis1() {
		for (int i = 0; i < ComparisonData.Expected_Tables_analysis1_rows.length; i++) {
			String row_xpath = "//div[contains(text(),'" + ComparisonData.Expected_Tables_analysis1_rows[i] + "')]";
			System.out.println(driver.findElement(By.xpath(row_xpath)).getText());
			Assert.assertTrue(driver.findElement(By.xpath(row_xpath)).isDisplayed());
		}
	}

	public void verfiy_rows_analysis2() {

	}

	// ---verify buttons and data in each table-------------------------
	public void find_AllDatacells() {
		List<WebElement> Data_cells = driver
				.findElements(By.xpath("//div[contains(@class,'ui-grid-cell ng-scope ui-grid-disable-selection')]"));
		for (int i = 0; i < Data_cells.size(); i++) {
			String data = Data_cells.get(i).getText();
			System.out.println(data);
			// Assert.assertFalse(data.isEmpty());
		}
	}

	public void verify_tables_buttons() {
		List<WebElement> firstTable_buttons = driver.findElements(By.xpath("//div[@id='SALES_FILTER']//button"));
		List<WebElement> secondTable_buttons = driver.findElements(By.xpath("//div[@id='TVA_FILTER']//button"));
		List<WebElement> thirdTable_buttons = driver.findElements(By.xpath("//div[@id='SUBSTITUTION_FILTER']//button"));
		List<WebElement> fourthTable_buttons = driver.findElements(By.xpath("//div[@id='FAMILY_FILTER']//button"));
		List<WebElement> tables_buttons = new ArrayList<WebElement>();
		tables_buttons.addAll(firstTable_buttons);
		tables_buttons.addAll(secondTable_buttons);
		tables_buttons.addAll(thirdTable_buttons);
		tables_buttons.addAll(fourthTable_buttons);
		for (int i = tables_buttons.size() - 1; i >= 0; i--) {
			tables_buttons.get(i).click();
			table_load_wait();
			find_AllDatacells();
			List<String> table_columns1 = gather_column();
			for (int j = 0; j < ComparisonData.columns_analysis1.length; j++) {
				Assert.assertEquals(ComparisonData.columns_analysis1[j], table_columns1.get(j));
			}
			verify_rows_analysis1();
		}
	}

	public void select_analysisType_withMDL() {
		analysis_type.click();
		driver.findElement(By.xpath("//div[contains(text(),'Comparatif de marge par tranche MDL')]")).click();
	}

	// ---------comparison segment filter-----------------
	public void verify_comparison_segmant_filter_options() {
		String segment_options = "//div[@id='TYPE_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		check_filter_works(comparison_segment, segment_options, "notreference", segment_used_filter_name, "standard");
	}

	// -----------------groupment filter----------
	public void select_comparison_segment_withGroupment() {
		comparison_segment.click();
		table_load_wait();
		driver.findElement(By.xpath("//div[contains(text(),'Groupement')]")).click();
	}

	public void verify_groupment_filter_default_option() {
		Groupment.click();
		String default_val = driver.findElement(By.xpath("(//div[contains(@class,'cg-sidenav-text-selected')])[3]"))
				.getText();
		String color = driver.findElement(By.xpath("(//div[contains(@class,'cg-sidenav-text-selected')])[3]"))
				.getCssValue("color");
		Assert.assertEquals(ComparisonData.Expected_comparison_default_filters[4], default_val);
		Assert.assertEquals(ComparisonData.Expected_default_filter_color, color);
	}

	public void verify_groupment_filter_options() {
		String groupment_options = "//div[@id='SUBGROUP_SELECT']//div[@class='cg-hover ng-scope layout-row']";
		check_filter_works(comparison_segment, groupment_options, "notreference", segment_used_filter_name, "standard");
	}

	// ---------period filter -----------
	public void verify_period_filter_options() {
		String period_options = "//div[contains(@id,'PERIOD')]//div[@class='cg-hover ng-scope layout-row']";
		check_filter_works(period, period_options, "notreference", period_used_filter_name, "standard");
	}

	public void verify_period_filter_optionsanalysis2() {
		String period_options = "//div[contains(@id,'PERIOD')]//div[@class='cg-hover ng-scope layout-row']";
		check_filter_works(period, period_options, "notreference", period_used_filter_name, "MDL");
	}

	public void verify_date_personalisee(String date_debut, String date_fin) {
		period.click();
		driver.findElement(By.xpath("//div[contains(text(),'Date personnalisée')]")).click();
		driver.findElement(By.xpath("//input[@placeholder='Date Début']")).sendKeys(date_debut);
		driver.findElement(By.xpath("//input[@placeholder='Date Fin']")).sendKeys(date_fin);
		table_load_wait();
		List<String> table_columns = gather_column();
		for (int j = 0; j < ComparisonData.column_analysis2.length; j++) {
			Assert.assertEquals(ComparisonData.column_analysis2[j], table_columns.get(j));
		}
	}

	// ---------Scenario6-----------
	public void verify_referenceperiod_filter_options() {
		String referenceperiod_options = "//div[@id='PERIOD_REFERENCE_SELECT']//div[@class='md-virtual-repeat-offsetter']//div";
		check_filter_works(reference_period, referenceperiod_options, "reference", period_used_filter_name, "standard");
	}

	// ---------Scenario7-----------
	public void verify_pharmacy_filter_options() {
		pharmacy.click();
		String newfirstElement_xpath = "//div[@id='PHARMA_SELECT']//div[@class='md-virtual-repeat-offsetter']//div[1]";
		String visibleList_xpath = "//div[@id='PHARMA_SELECT']//div[@class='md-virtual-repeat-offsetter']//div";
		String scroll_xpath = "(//div[@class='md-virtual-repeat-scroller'])[6]";
		filter_container(newfirstElement_xpath, visibleList_xpath, scroll_xpath, 100, "standard");
	}

//	public void verify_pharmacy_filter_dupliaction() {
//		pharmacy.click();
//		String newfirstElement_xpath = "//div[@id='PHARMA_SELECT']//div[@class='md-virtual-repeat-offsetter']//div[1]";
//		String visibleList_xpath = "//div[@id='PHARMA_SELECT']//div[@class='md-virtual-repeat-offsetter']//div";
//		String scroll_xpath = "(//div[@class='md-virtual-repeat-scroller'])[6]";
//		filter_container(newfirstElement_xpath, visibleList_xpath, scroll_xpath, 50, "standard");
//	}

	public void verify_pharmacy_filter_optionsanalysisMDL() {
		pharmacy.click();
		String newfirstElement_xpath = "//div[@id='PHARMA_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')][1]";
		String visibleList_xpath = "//div[@id='PHARMA_SELECT']//div[contains(@class,'cg-hover ng-scope layout-row')]";
		String scroll_xpath = "(//div[@class='md-virtual-repeat-scroller'][@role='presentation'])[6]//div";
		filter_container(newfirstElement_xpath, visibleList_xpath, scroll_xpath, 100, "MDL");
	}

	// ---------------------------------------------------
	public List<Boolean> verify_pharmacy_searchSuggestedVals(String search_val) {
		String Suggested_values_xpath = "//div[@id='PHARMA_SELECT']//div[@class='cg-hover ng-scope layout-row']";
		List<Boolean> Pharmacies_search_check = SharedMethodsElements.check_filter_search_suggested_values(pharmacy,
				Pharmacy_search_box, Suggested_values_xpath, search_val);
		return Pharmacies_search_check;
	}
	
	public void verify_pharmacy_selectedVal(String search_val ,String Selected_val) {
		List<WebElement> suggested_val = driver
				.findElements(By.xpath("//div[@id='PHARMA_SELECT']//div[@class='cg-hover ng-scope layout-row']"));
		int flag=0;
		for (int i = 0; i < suggested_val.size(); i++) {
			if (suggested_val.get(i).getText().contentEquals(Selected_val)) {
				System.out.println("selected value"+Selected_val);
				suggested_val.get(i).click();
				Assert.assertTrue(pharmacy_used_filter_name.getText().contains(Selected_val));
				flag++;
			}
		}
		
		if(flag==0) {
			System.out.println("----------value not found-----------");
		}
		
	}

	public void verify_pharmacy_search(String search_val, String selected_val) {
		List<WebElement> suggested_val = driver
				.findElements(By.xpath("//div[@id='PHARMA_SELECT']//div[@class='cg-hover ng-scope layout-row']"));
		Assert.assertTrue(suggested_val.size() > 0);
		for (int i = 0; i < suggested_val.size(); i++) {
			String suggested_values = suggested_val.get(i).getText();
			System.out.println(suggested_values);
			Assert.assertTrue(suggested_values.contains(search_val));
			if (suggested_values.contentEquals(selected_val)) {
				suggested_val.get(i).click();
				Assert.assertTrue(pharmacy_used_filter_name.getText().contains(selected_val));
			} else {
				System.out.println("value not found");
			}
		}
	}

	// ------Scenario9-----------------
	public void verify_csvfiles_download() {
		filters_menu.click();
		By loadingProgress = By.xpath("div[@ng-show=\"chart.loading\"]");
		UtilityClass.waitExpectedConditionsElementContainsAttributeText(loadingProgress, "aria-hidden", "true");
		List<WebElement> download_csvIcons = driver.findElements(By.id("GRID_EXPORT_CSV"));
		System.out.println(download_csvIcons.size());
		for (int i = 0; i < download_csvIcons.size(); i++) {
			download_csvIcons.get(i).click();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			UtilityClass.check_downloadedFile_size(i);
		}
	}

	public void verify_pdfFile_download() {
		driver.findElement(By.xpath("//button[@id='GRID_EXPORT_PDF']")).click();
		// switch between tabs
		new Actions(driver).sendKeys(driver.findElement(By.tagName("html")), Keys.CONTROL)
				.sendKeys(driver.findElement(By.tagName("html")), Keys.NUMPAD2).build().perform();
		new Actions(driver).sendKeys(driver.findElement(By.tagName("html")), Keys.CONTROL)
				.sendKeys(driver.findElement(By.tagName("html")), Keys.NUMPAD1).build().perform();
	}

	public void readExcel(String filePath, String fileName, String sheetName) throws IOException {
		File file = new File(filePath + "\\" + fileName);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook guru99Workbook = null;
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			guru99Workbook = new XSSFWorkbook(inputStream);
		} else if (fileExtensionName.equals(".xls")) {
			guru99Workbook = new HSSFWorkbook(inputStream);
		}
		Sheet guru99Sheet = guru99Workbook.getSheet(sheetName);
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 0; i < rowCount + 1; i++) {
			Row row = guru99Sheet.getRow(i);
			// Create a loop to print cell values in a row
			for (int j = 0; j < row.getLastCellNum(); j++) {
				System.out.print(row.getCell(j).getStringCellValue() + "|| ");
			}
			System.out.println();
		}
	}

	public void check_filter_works(WebElement filter, String Options_xpath, String filtername, WebElement data_check,
			String analysis_type) {
		filter.click();
		List<WebElement> filter_options = driver.findElements(By.xpath(Options_xpath));
		for (int i = 0; i < filter_options.size(); i++) {
			UtilityClass.set_javaSc_executor(filter_options.get(i));
			System.out.println("filter option----" + filter_options.get(i).getText());
//			filter_options.get(i).click();
			table_load_wait();
			if (analysis_type.contentEquals("standard")) {
				// check table columns
				List<String> table_columns = gather_column();
				System.out.println("column size " + table_columns.size());
				System.out.println("column expected " + ComparisonData.columns_analysis1.length);
				for (int j = 0; j < table_columns.size(); j++) {
					Assert.assertEquals(ComparisonData.columns_analysis1[j], table_columns.get(j));
				}
				// check table rows
				verify_rows_analysis1();
			} else {
				// check table columns
				List<String> table_columns = gather_column();
				for (int j = 0; j < ComparisonData.column_analysis2.length; j++) {
					Assert.assertEquals(ComparisonData.column_analysis2[j], table_columns.get(j));
				}
				// check table rows
				verfiy_rows_analysis2();
			}
			if (filtername.contentEquals("notreference")) {
				// check used filter name at the top of table
				Assert.assertTrue(data_check.getText().contains(filter_options.get(i).getText()));
			}
		}
	}

	// ----------------------------------filter_container---------------
	int x = 0;
	public List<String> filter_container(String newfirst_elment_xpath, String visibleList_xpath, String scroll_xpath,
			int Scroll_pixels, String filter) {
		List<String> elementsList = new ArrayList<String>();
		String firstElementText = "";
		String newFirstElementText = "";
		Boolean listFinished = false;
		while (!listFinished) {
			newFirstElementText = driver.findElement(By.xpath(newfirst_elment_xpath)).getText();
			System.out.println("newFirsteleement " + newFirstElementText);
			if (newFirstElementText.equals(firstElementText)) {
				System.out.println("newFirsteleement " + newFirstElementText);
				if (!firstElementText.equals("")) {
					listFinished = true;
				}
			} else {
				List<WebElement> visibleList = driver.findElements(By.xpath(visibleList_xpath));
				for (int i = 0; i < visibleList.size(); i++) {
					if (!elementsList.contains(visibleList.get(i).getText())) {
						elementsList.add(visibleList.get(i).getText());
						System.out.println("element ---" + visibleList.get(i).getText());
						UtilityClass.set_javaSc_executor(visibleList.get(i));
						table_load_wait();
						if (filter.contentEquals("standard")) {
							// check table columns
							List<String> table_columns = gather_column();
							for (int j = 0; j < ComparisonData.columns_analysis1.length; j++) {
								Assert.assertEquals(ComparisonData.columns_analysis1[j], table_columns.get(j));
							}
							// check table rows
							verify_rows_analysis1();
						} else {
							// check table columns
							List<String> table_columns = gather_column();
							for (int j = 0; j < ComparisonData.column_analysis2.length; j++) {
								Assert.assertEquals(ComparisonData.column_analysis2[j], table_columns.get(j));
							}
							// check table rows
							verfiy_rows_analysis2();
						}
					}
				}
				firstElementText = newFirstElementText;
				System.out.println("first element " + firstElementText);
				System.out.println("visible list size " + visibleList.size());
				System.out.println("scroll");
				UtilityClass.scroll_down(scroll_xpath, Scroll_pixels);
				try {
					Thread.sleep(50000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return elementsList;
	}

}
