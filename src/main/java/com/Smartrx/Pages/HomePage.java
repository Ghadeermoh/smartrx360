package com.Smartrx.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.Smartrx.Utility.UtilityClass;

public class HomePage extends BaseClass {

	@FindBy(name = "email")
	WebElement Email;
	@FindBy(name = "password")
	WebElement Password;
	@FindBy(xpath = "//md-select-value[contains(@id,'select_value_label')]")
	WebElement group_select;

	@FindBy(xpath = "//button[@type='submit']")
//   @FindBy (tagName ="button")
	WebElement LoginBtn;

	// initiate elements
	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public void Login(String un, String pass) {
		Email.clear();
		Email.sendKeys(un);
		Password.clear();
		Password.sendKeys(pass);
		UtilityClass.set_javaSc_executor(LoginBtn);
//		groument_selection(Groupment_Name);
	}

	public void groument_selection(String Groupment_Name) {
		UtilityClass.set_javaSc_executor(group_select);
		String Group_xpath = "//div[contains(text(),'" + Groupment_Name + "')]";
		System.out.println(Group_xpath);
		WebElement groupment = driver.findElement(By.xpath(Group_xpath));
		UtilityClass.set_javaSc_executor(groupment);
	}

}
